<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld"%>

<footer>
	<p> <ctg:copyright/> <fmt:message key="pages.footer.copyright" bundle="${rb}" /> </p>
</footer>
