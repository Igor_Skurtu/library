<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/jsp/common/choose_language.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/style.css">
<title><fmt:message key="pages.registration.sussecc.title" bundle="${rb}" /></title>
</head>

<body>

	<div id="page_layout">
		<header>
			<a href="http://localhost:8080/Library"><img id="logo" src="image/logo.png" /></a>
		</header>




	<main>
		<div id="granny_container">
			<img id="error_granny_img" src="image/error.jpg" />
			<p id="granny_says"> <fmt:message key="pages.registration.sussecc.message" bundle="${rb}" /> 	</p>
			<form id="form_error" action="index.jsp" method="POST">
				<input type="submit" value="&lt;&nbsp;&lt;&nbsp;&lt;&nbsp;&lt;&nbsp;&lt;" />
			</form>
		</div>

	</main>


		
	</div>

</body>
</html>