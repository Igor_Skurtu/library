<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="true"%>

<%@ include file="/jsp/common/choose_language.jsp"%>

<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">

	  <!-- CSS -->
	  <link rel="stylesheet" href="css/style.css">
	  <title> <fmt:message key="pages.registration.title" bundle="${rb}" /> </title>
	</head>
	<body>
		<div id="page_layout">
			<header>
				<a href="http://localhost:8080/Library"><img id="logo" src="image/logo.png"/></a>

			</header>
		
					<form class="form_login" name="registration" method="POST" action="controller?param=registration">
					
					<label for="name"> <fmt:message key="pages.registration.name" bundle="${rb}" />	</label>
					<input id="name" type="text" name="name" pattern="^((\w|[а-яА-Я]|[ .,?:!]){1,40})$" title="MAX: 40" required/>
					
					<label for="surname"> <fmt:message key="pages.registration.surname" bundle="${rb}" /> </label>
					<input id="surname" type="text" name="surname" pattern="^((\w|[а-яА-Я]|[ .,?:!]){1,40})$" title="MAX: 40" required/>
					
					<label for="email"><fmt:message key="pages.registration.email" bundle="${rb}" /></label>
					<input id="email" type="email" name="email" required/>
					
					<label for="password"><fmt:message key="pages.registration.pass" bundle="${rb}" /></label>
					<input id="password" type="password" name="password" pattern="^(\w{5,20})$" title="5-20" required/>
					
					<label for="re_password"><fmt:message key="pages.registration.re_pass" bundle="${rb}" /></label>
					<input id="re_password" type="password" name="re_password" pattern="^(\w{5,20})$" title="5-20" required/>
					
					<input class="submit" type="submit" value= <fmt:message key="pages.registration.register" bundle="${rb}" /> />

				</form>
			
			<%@ include file="/jsp/common/footer.jsp"%>

		</div>
	</body>
</html>