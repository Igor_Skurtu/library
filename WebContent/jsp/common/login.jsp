﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/jsp/common/choose_language.jsp"%>

<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">

	  <!-- CSS -->
	  <link rel="stylesheet" href="css/style.css">
	  <title> <fmt:message key="pages.login.title" bundle="${rb}" /> </title>
	</head>
	<body>
		<div id="page_layout">
			<header>
				<a href="http://localhost:8080/Library"><img id="logo" src="image/logo.png"/></a>
				<div id="flags">
					<a href="controller?param=change_locale&lang=ru"><img src="image/flag/ru.png"/></a>
					<a href="controller?param=change_locale&lang=en"><img src="image/flag/en.png"/></a>
				</div>
			</header>
		
				<form class="form_login" name="login" method="POST" action="controller?param=sign_in">
					
					<label for="email"> 
						<fmt:message key="pages.login.email" bundle="${rb}" /> 
					</label>
					<input id="email" type="email" value="${email}" name="email" required/>
					
					<label for="password"><fmt:message key="pages.login.pass" bundle="${rb}" /></label>
					<input id="password" type="password" name="password" pattern="^(\w{5,20})$" title="5-20" required/>
					
					<input class="submit" type="submit" value= <fmt:message key="pages.login.sign_in" bundle="${rb}" /> />
					<c:if test="${user_does_not_exist}">
							<span id="login_error"><fmt:message key="pages.login.error.email" bundle="${rb}" /></span>
					</c:if>
					<c:if test="${incorrect_password}">
							<span id="login_error"><fmt:message key="pages.login.error.pass" bundle="${rb}" /></span>
					</c:if>
					<div class="register"><a href="/Library/controller?param=path_registration"> <fmt:message key="pages.login.register" bundle="${rb}" /> </a></div>
				</form>
				
			
			<%@ include file="/jsp/common/footer.jsp"%>

		</div>
	</body>
</html>