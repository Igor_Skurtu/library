<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="search_book_form">
	<form method="POST" action="/Library/controller?param=search_books">

		<!--------------------------------------------AUTHOR_SELECT------------------------------------------------------------------------------------->
		<div class="column_in_form">
			<label for="bauthor"><fmt:message
					key="pages.admin.books.search.author_surname" bundle="${rb}" /></label> <select
				id="bauthor" name="bauthor">
				<option value="-1"><fmt:message
						key="pages.admin.books.search.authors_list.all" bundle="${rb}" /></option>

				<c:forEach var="author" items="${sessionScope.authors}">

					<c:if
						test="${(not empty sessionScope.bauthor) and (sessionScope.bauthor == author.id)}">
						<option selected value="${author.id}">${author.surname},&nbsp;${author.name}</option>
					</c:if>
					<c:if
						test="${(not empty sessionScope.bauthor) and  (sessionScope.bauthor != author.id)}">
						<option value="${author.id}">${author.surname},&nbsp;${author.name}</option>
					</c:if>
					<c:if test="${empty sessionScope.bauthor}">
						<option value="${author.id}">${author.surname},&nbsp;${author.name}</option>
					</c:if>

				</c:forEach>

			</select>
		</div>



		<!---------------------------------------------------------------------------------------------------------------------------------------------->

		<!--------------------------------------------BOOK_TITLE_INPUT---------------------------------------------------------------------------------->

		<div class="column_in_form">
			<label for="bname"><fmt:message
					key="pages.admin.books.search.book_title" bundle="${rb}" /></label> <input
				id="bname" type="text" name="bname" value="${sessionScope.bname}" pattern="^((\w|[а-яА-Я]|[ .,?:!]){1,60})$" title="MAX: 60"/>
		</div>
		<!---------------------------------------------------------------------------------------------------------------------------------------------->

		<!--------------------------------------------CATEGORY_SELECT----------------------------------------------------------------------------------->



		<div class="column_in_form">
			<label for="bcategory"><fmt:message
					key="pages.admin.books.search.category" bundle="${rb}" /></label> <select
				id="bcategory" name="bcategory">

				<option value="-1"><fmt:message
						key="pages.admin.books.search.category_list.all" bundle="${rb}" /></option>




				<c:choose>
					<c:when
						test="${sessionScope.locale eq 'ru' or empty sessionScope.locale}">
						<c:forEach var="category" items="${sessionScope.categories}">
							<c:if
								test="${(not empty sessionScope.bcategory) and (sessionScope.bcategory == category.id)}">
								<option selected value="${category.id}">${category.nameRu}</option>
							</c:if>
							<c:if
								test="${(not empty sessionScope.bcategory) and  (sessionScope.bcategory != category.id)}">
								<option value="${category.id}">${category.nameRu}</option>
							</c:if>
							<c:if test="${empty sessionScope.bcategory}">
								<option value="${category.id}">${category.nameRu}</option>
							</c:if>
						</c:forEach>
					</c:when>


					<c:when test="${sessionScope.locale eq 'en'}">
						<c:forEach var="category" items="${sessionScope.categories}">
							<c:if
								test="${(not empty sessionScope.bcategory) and (sessionScope.bcategory == category.id)}">
								<option selected value="${category.id}">${category.nameEn}</option>
							</c:if>
							<c:if
								test="${(not empty sessionScope.bcategory) and (sessionScope.bcategory != category.id)}">
								<option value="${category.id}">${category.nameEn}</option>
							</c:if>
							<c:if test="${empty sessionScope.bcategory}">
								<option value="${category.id}">${category.nameEn}</option>
							</c:if>
						</c:forEach>
					</c:when>
				</c:choose>



			</select>
		</div>
		<input class="submit" type="submit"
			value=<fmt:message key="pages.admin.books.search.search_button" bundle="${rb}" /> />
	</form>
</div>











