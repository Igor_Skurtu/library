<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>




<%@ include file="/jsp/common/search_form.jsp"%>

<!-----------------------OPERATION RESULT--------------------------------------------------------------------->
		<c:if test="${sessionScope.add_to_order_operation_result}">
		<center><fmt:message key="pages.user.books.add_to_order_message" bundle="${rb}" /></center>
		</c:if>
<!------------------------------------------------------------------------------------------------------------>

<!-----------------------SEARCH RESULTS----------------------------------------------------------------------->

			<c:if test="${not empty sessionScope.books}">

				<form class="add_book_in_order_form" action="/Library/controller?param=add_to_order" method="POST">
					<table class="book_table">
						<caption>
							<fmt:message key="pages.user.books.search_results.message"
								bundle="${rb}" />
						</caption>

						<tr>
							<th class="book_author"><fmt:message
									key="pages.user.books.search_results.author" bundle="${rb}" /></th>
							<th class="book_name"><fmt:message
									key="pages.user.books.search_results.book_title" bundle="${rb}" /></th>
							<th class="book_edit"><fmt:message
									key="pages.user.books.search_results.category" bundle="${rb}" /></th>
							<th class="book_edit"><fmt:message
									key="pages.user.books.search_results.amount" bundle="${rb}" /></th>
							<th class="book_del"><fmt:message
									key="pages.user.books.search_results.order" bundle="${rb}" /></th>
						</tr>

						<c:forEach var="book" items="${sessionScope.books}">

							<tr>
								<td>${book.authorString}</td>
								<td>${book.title}</td>
								
<!--------------------------------------- Localized category output -->
								<c:choose>
									<c:when test="${sessionScope.locale eq 'ru' or empty sessionScope.locale}">
										<td>${book.categoryStringRu}</td>
									</c:when>
									<c:when test="${sessionScope.locale eq 'en'}">
										<td>${book.categoryStringEn}</td>
									</c:when>
								</c:choose>
<!------------------------------------------------------------------>			
								<td>${book.amount}</td>
								
								<c:if test="${book.amount != 0}">
									<td><input class="add_book_check" type="checkbox"
										name="add" value="${book.id}"></td>
								</c:if>
								
								<c:if test="${book.amount == 0}"><td></td></c:if>
								
							</tr>

						</c:forEach>
					</table>
					<div id="add_book_in_order">
						<input class="submit" type="submit"
							value=<fmt:message key="pages.user.books.search_results.add" bundle="${rb}" /> />
					</div>
				</form>
				<p id="pcs_checked" class="text_under_table">&nbsp;</p>

			</c:if>