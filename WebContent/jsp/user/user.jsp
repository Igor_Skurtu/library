<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<!-- CSS -->
<link rel="stylesheet" href="css/style.css">
<title><fmt:message key="pages.user.title" bundle="${rb}" /></title>
</head>
<body>
	<div id="page_layout">
		<header>
			<a href="http://localhost:8080/Library"><img id="logo"
				src="image/logo.png" /></a>
			<div id="flags">
				<a href="controller?param=change_locale&lang=ru"><img
					src="image/flag/ru.png" /></a> <a
					href="controller?param=change_locale&lang=en"><img
					src="image/flag/en.png" /></a>
			</div>
			<a id="logout" href="controller?param=sign_out"> <fmt:message
					key="pages.header.logout" bundle="${rb}" />
			</a>
			<c:choose>
				<c:when test="${sessionScope.user_layout eq 'books'}">
					<c:set var="books_menu_class" value="youarehere" scope="page" />
					<c:remove var="orders_menu_class" />
				</c:when>
				<c:when test="${sessionScope.user_layout eq 'orders'}">
					<c:set var="orders_menu_class" value="youarehere" scope="page" />
					<c:remove var="books_menu_class" />
				</c:when>
			</c:choose>
			<nav id="main_menu">
				<ul class="two_point_menu">
					<li class="${books_menu_class}"><a
						href="/Library/controller?param=user_books"><fmt:message
								key="pages.user.books.menu" bundle="${rb}" /></a></li>
					<li class="${orders_menu_class}"><a
						href="/Library/controller?param=user_orders"><fmt:message
								key="pages.user.orders.menu" bundle="${rb}" /></a></li>
				</ul>
			</nav>
		</header>
		<main>
		<p>
			<ctg:userinfo />
		</p>

		<!-----------------------BOOKS LAYOUT------------------------------------------------------------------>
		<c:if test="${sessionScope.user_layout eq 'books'}">


			<%@ include file="/jsp/user/user_books.jsp"%>

		</c:if> <!----------------------------------------------------------------------------------------------------------->



		<!-----------------------ORDERS LAYOUT----------------------------------------------------------------------->

		<c:if test="${sessionScope.user_layout eq 'orders'}">

		<%@ include file="/jsp/user/user_orders.jsp"%>

		</c:if> <!----------------------------------------------------------------------------------------------------------->

		</main>
		<%@ include file="/jsp/common/footer.jsp"%>
	</div>



	<script type="text/javascript">
		//counting checkboxes
		var chs = document.getElementsByClassName("add_book_check");
		for (var i = 0; i < chs.length; i++) {
			chs[i].onclick = count;
		}

		function count() {
			var checkboxes = document.getElementsByClassName("add_book_check");
			var checked = 0;
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].checked) {
					checked += 1;
				}
				//alert(checkboxes[i].checked);
			}
			document.getElementById('pcs_checked').innerHTML = "Выбрано книг:"
					+ checked + ".";
		}
	</script>

	<script type="text/javascript">
		//deleting confirm 
		var del_btns = document.getElementsByClassName("btn_del");
		for (var i = 0; i < del_btns.length; i++) {
			del_btns[i].onclick = function() {
				var conf = confirm("Подтвердите удаление");
				if (!conf) {
					return false;
				}
			};
		}
	</script>
</body>
</html>