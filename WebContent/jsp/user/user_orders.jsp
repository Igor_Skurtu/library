<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!-- если никаких заказов нет - сообщение об этом и инструкции по заказу -->

<c:if test="${not empty sessionScope.user_orders}">

	<c:forEach var="order" items="${sessionScope.user_orders}">
		<c:if test="${not order.sent }">
			<c:set var="forSending" value="true" scope="page"></c:set>
		</c:if>
		<c:if test="${order.sent and not order.proceeded}">
			<c:set var="alreadySent" value="true" scope="page"></c:set>
		</c:if>
		<c:if test="${order.sent and order.proceeded}">
			<c:set var="proceeded" value="true" scope="page"></c:set>
		</c:if>
	</c:forEach>

</c:if>
<!-------------------------------------------ORDERS_FOR_SENDING-------------------------------------------------------------------->

<c:if test="${forSending eq 'true' }">

	<form class="add_book_in_order_form"
		action="controller?param=make_order" method="POST">
		<table class="book_table">
			<caption>Заказы для отправки</caption>
			<tr>
				<th class="book_author">Автор</th>
				<th class="book_name_small">Название</th>
				<th>Куда</th>
				<th class="book_del">Удалить</th>
			</tr>

			<c:forEach var="order" items="${sessionScope.user_orders}">
				<c:if test="${not order.sent}">
					<tr>
						<td>${order.authorString}</td>
						<td>${order.bookString}</td>

						<td><input type="radio" name="place${order.id}" value="1"
							checked> читальный зал <input type="radio"
							name="place${order.id}" value="0"> на руки</td>

						<td><a class="btn_del"
							href="controller?param=delete_order&orderId=${order.id}" /></td>

					</tr>
				</c:if>
			</c:forEach>

		</table>
		<div id="add_book_in_order">
			<input class="submit" type="submit" value="Сделать заказ" />
		</div>

	</form>

	<a href="controller?param=clear_order_list" class="text_under_table">Очистить
		список</a>

</c:if>
<!------------------------------------------------------------------------------------------------------------------------------->


<!-------------------------------------------SENT_ORDERS------------------------------------------------------------------------->

<c:if test="${alreadySent eq 'true' }">

	<table class="book_table">
		<caption>Отправленные заказы</caption>
		<tr>
			<th class="book_author">Автор</th>
			<th class="book_name_small">Название</th>
			<th>Куда</th>
			<th>Время</th>
			<th class="book_del">Удалить</th>
		</tr>

		<c:forEach var="order" items="${sessionScope.user_orders}">
			<c:if test="${order.sent and not order.proceeded}">
				<tr>
					<td>${order.authorString}</td>
					<td>${order.bookString}</td>
					
					<td>
						<c:if test="${order.hall}">Читальный зал</c:if>
                    	<c:if test="${not order.hall}">На руки </c:if>
                    </td>
					
					<td><fmt:formatDate type="BOTH" value="${order.dateTime}" /></td>
					
					<td><a class="btn_del" href="controller?param=delete_order&orderId=${order.id}" /></td>

				</tr>
			</c:if>
		</c:forEach>

	</table>

</c:if>
<!------------------------------------------------------------------------------------------------------------------------------->


<!-------------------------------------------OPEN_ORDERS------------------------------------------------------------------------->

<c:if test="${proceeded eq 'true' }">

	<table class="book_table">
		<caption>Открытые заказы</caption>
		<tr>
			<th class="book_author">Автор</th>
			<th class="book_name_small">Название</th>
			<th>Куда</th>
			<th>Когда</th>
			<th>Вернуть до</th>
		</tr>

		<c:forEach var="order" items="${sessionScope.user_orders}">
			
			<c:if test="${order.proceeded}">
			
			<!-- SET TWO WEEKS TERM TO BOOK RETURN -->			
			<c:if test="${not order.hall}">
				<jsp:useBean id="returnDateTime" scope="page" class="java.util.Date" />			
				<jsp:setProperty name="returnDateTime" property="time" value="${order.dateTime.time + 1209600000}"/> 
			</c:if>
			
				<tr>
					<td>${order.authorString}</td>
					<td>${order.bookString}</td>
					
					<td>
						<c:if test="${order.hall}">Читальный зал</c:if>
                    	<c:if test="${not order.hall}">На руки </c:if>
                    </td>
					
					<td><fmt:formatDate type= "BOTH" value= "${order.dateTime}" /></td>
					<!-- DISPLAY TWO WEEKS TERM TO BOOK RETURN ONLY FOR HOME ORDERS-->
					<c:if test="${not order.hall}">
						<td><fmt:formatDate type= "BOTH" value= "${returnDateTime}" /></td>
					</c:if>
					<c:if test="${order.hall}">
						<td>&nbsp;</td>
					</c:if>
				</tr>
			</c:if>
		</c:forEach>

	</table>

</c:if>
<!------------------------------------------------------------------------------------------------------------------------------->

