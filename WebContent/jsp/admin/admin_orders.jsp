<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>




<c:if test="${not empty sessionScope.all_orders}">

	<c:forEach var="order" items="${sessionScope.all_orders}">
		<c:if test="${order.proceeded}">
			<c:set var="proceeded" value="true" scope="page"></c:set>
		</c:if>
	</c:forEach>

</c:if>

<c:if test="${proceeded eq 'true' }">
	<table>
		<caption>Новые заказы</caption>
			<tr>
				<th class="book_author">Пользователь</th>
				<th class="book_name_small">Автор</th>
				<th class="book_name_small">Название</th>
				<th>Куда</th>
				<th>Время выдачи</th>
				<th>Возврат до</th>
				<th class="book_del">Закрыть заказ</th>
			</tr>	
		<c:forEach var="order" items="${sessionScope.all_orders}">
			<c:if test="${order.proceeded}">
			
			
			<!-- SET TWO WEEKS TERM TO BOOK RETURN -->			
			<c:if test="${not order.hall}">
				<jsp:useBean id="returnDateTime" scope="page" class="java.util.Date" />			
				<jsp:setProperty name="returnDateTime" property="time" value="${order.dateTime.time + 1209600000}"/> 
			</c:if>
			
			
			<tr>
				<td>${order.userString}</td>
				<td>${order.authorString}</td>
				<td>${order.bookString}</td>
				<td>
					<c:if test="${order.hall}">Чит. зал</c:if>
                    <c:if test="${not order.hall}">На руки </c:if>
                </td>
				<td><fmt:formatDate type="BOTH" value="${order.dateTime}" /></td>
				
				<!-- DISPLAY TWO WEEKS TERM TO BOOK RETURN ONLY FOR HOME ORDERS-->
					<c:if test="${not order.hall}">
						<td><fmt:formatDate type= "BOTH" value= "${returnDateTime}" /></td>
					</c:if>
					<c:if test="${order.hall}">
						<td>&nbsp;</td>
					</c:if>
				
				<td><a class="btn_del" href="controller?param=delete_order&orderId=${order.id}" /></td>
			</tr>
			</c:if>
		</c:forEach>
	</table>
</c:if>

