<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:if test="${not empty sessionScope.all_orders}">

	<c:forEach var="order" items="${sessionScope.all_orders}">
		<c:if test="${order.sent and not order.proceeded}">
			<c:set var="forProceeding" value="true" scope="page"></c:set>
		</c:if>
	</c:forEach>

</c:if>

<c:if test="${forProceeding eq 'true' }">
	<table>
		<caption>Новые заказы</caption>
			<tr>
				<th class="book_author">Пользователь</th>
				<th class="book_name_small">Автор</th>
				<th class="book_name_small">Название</th>
				<th class="book_edit">Доступно</th>
				<th>Куда</th>
				<th>Время</th>
				<th class="book_del">Выполнить</th>
				<th class="book_del">Отклонить</th>
			</tr>	
		<c:forEach var="order" items="${sessionScope.all_orders}">
			<c:if test="${order.sent and not order.proceeded}">
			<tr>
				<td>${order.userString}</td>
				<td>${order.authorString}</td>
				<td>${order.bookString}</td>
				<td>${order.bookAmount}</td>
				<td>
					<c:if test="${order.hall}">Чит. зал</c:if>
                    <c:if test="${not order.hall}">На руки </c:if>
                </td>
				<td><fmt:formatDate type="BOTH" value="${order.dateTime}" /></td>
				<td><a class="btn_proc" href="controller?param=proc_order&orderId=${order.id}&bookId=${order.book}" /></td>
				<td><a class="btn_del" href="controller?param=delete_order&orderId=${order.id}" /></td>
			</tr>
			</c:if>
		</c:forEach>
	</table>
</c:if>
