<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



			<%@ include file="/jsp/common/search_form.jsp"%>




			<!-----------------------OPERATION RESULT--------------------------------------------------------------------->
			<c:if test="${sessionScope.delete_book_operation_result}">
				<center>
					<fmt:message key="pages.admin.books.delete.success_message"
						bundle="${rb}" />
				</center>
			</c:if>
			<!------------------------------------------------------------------------------------------------------------>





			<!-----------------------SEARCH RESULTS----------------------------------------------------------------------->

			<c:if test="${not empty sessionScope.books}">

				<form class="add_book_in_order_form"
					action="/Library/controller?param=add_to_order" method="POST">
					<table class="book_table">
						<caption>
							<fmt:message key="pages.admin.books.search_results.message"
								bundle="${rb}" />
						</caption>

						<tr>
							<th class="book_author"><fmt:message
									key="pages.admin.books.search_results.author" bundle="${rb}" /></th>
							<th class="book_name"><fmt:message
									key="pages.admin.books.search_results.book_title"
									bundle="${rb}" /></th>
							<th class="book_edit"><fmt:message
									key="pages.admin.books.search_results.category" bundle="${rb}" /></th>
							<th class="book_edit"><fmt:message
									key="pages.admin.books.search_results.amount" bundle="${rb}" /></th>
							<th class="book_edit"><fmt:message
									key="pages.admin.books.search_results.edit" bundle="${rb}" /></th>
							<th class="book_del"><fmt:message
									key="pages.admin.books.search_results.delete" bundle="${rb}" /></th>
						</tr>

						<c:forEach var="book" items="${sessionScope.books}">

							<tr>
								<td>${book.authorString}</td>
								<td>${book.title}</td>

								<!--------------------------------------- Localized category output -->
								<c:choose>
									<c:when
										test="${sessionScope.locale eq 'ru' or empty sessionScope.locale}">
										<td>${book.categoryStringRu}</td>
									</c:when>
									<c:when test="${sessionScope.locale eq 'en'}">
										<td>${book.categoryStringEn}</td>
									</c:when>
								</c:choose>
								<!------------------------------------------------------------------>
								<td>${book.amount}</td>

								<td><a class="btn_edit"
									href="controller?param=book_to_edit&bookid=${book.id}" /></td>
								<td><a class="btn_del"
									href="controller?param=delete_book&bookid=${book.id}" /></td>



							</tr>

						</c:forEach>
					</table>

				</form>

			</c:if>

			<!----------------------------------------------------------------------------------------------------------->





			<!-------------------------------------------------ADD/EDIT BOOK FORM-------------------------------------->
			<!-----------------------OPERATION RESULT--------------------------------------------------------------------->
			<c:if test="${sessionScope.add_edit_book_operation_result}">
				<center>
					<fmt:message key="pages.admin.books.add_edit_message"
						bundle="${rb}" />
				</center>
			</c:if>
			<!------------------------------------------------------------------------------------------------------------>


			<div id="add_book_form">


				<c:if test="${empty sessionScope.book_to_edit}">
					<form method="POST" action="controller?param=add_book">
				</c:if>

				<c:if test="${not empty sessionScope.book_to_edit}">
					<form method="POST" action="controller?param=edit_book&bookid=${sessionScope.book_to_edit.id}">
				</c:if>


				<div class="column_in_form">
					<label for="bauthor">Название книги</label> <input id="bauthor"
						type="text" name="bname" value="${book_to_edit.title}" pattern="^((\w|[а-яА-Я]|[ .,?:!]){1,60})$" title="MAX: 60"/>
				</div>


				<!------------------------------------------------------------------------------------------------------------------->

				<div class="column_in_form">
					<label for="bcategory">Автор</label> <select id="bcategory"
						name="bauthor">
						<!-------------------------------SELECT NEEDED AUTHOR FOR BOOK EDITING------------------------------------------------------------------------------------>
						<c:forEach var="author" items="${sessionScope.authors}">

							<c:if
								test="${(not empty sessionScope.book_to_edit) and (book_to_edit.author == author.id)}">
								<option selected value="${author.id}">${author.surname},&nbsp;${author.name}</option>
							</c:if>
							<c:if
								test="${(not empty sessionScope.book_to_edit) and  (book_to_edit.author != author.id)}">
								<option value="${author.id}">${author.surname},&nbsp;${author.name}</option>
							</c:if>
							<c:if test="${empty sessionScope.book_to_edit}">
								<option value="${author.id}">${author.surname},&nbsp;${author.name}</option>
							</c:if>

						</c:forEach>
						<!------------------------------------------------------------------------------------------------------------------------------------------------------->
					</select>
				</div>

				<div class="column_in_form">
					<label for="bcategory">Категория</label> <select id="bcategory"
						name="bcategory">

						<!---------------Localized category output for category select + DYNAMIC SELECT FOR BOOK EDITING--------------------------------------->
						<c:choose>
							<c:when
								test="${sessionScope.locale eq 'ru' or empty sessionScope.locale}">
								<c:forEach var="category" items="${sessionScope.categories}">
									<c:if
										test="${(not empty sessionScope.book_to_edit) and (book_to_edit.category == category.id)}">
										<option selected value="${category.id}">${category.nameRu}</option>
									</c:if>
									<c:if
										test="${(not empty sessionScope.book_to_edit) and  (book_to_edit.category != category.id)}">
										<option value="${category.id}">${category.nameRu}</option>
									</c:if>
									<c:if test="${empty sessionScope.book_to_edit}">
										<option value="${category.id}">${category.nameRu}</option>
									</c:if>
								</c:forEach>
							</c:when>


							<c:when test="${sessionScope.locale eq 'en'}">
								<c:forEach var="category" items="${sessionScope.categories}">
									<c:if
										test="${(not empty sessionScope.book_to_edit) and (book_to_edit.category == category.id)}">
										<option selected value="${category.id}">${category.nameEn}</option>
									</c:if>
									<c:if
										test="${(not empty sessionScope.book_to_edit) and (book_to_edit.category != category.id)}">
										<option value="${category.id}">${category.nameEn}</option>
									</c:if>
									<c:if test="${empty sessionScope.book_to_edit}">
										<option value="${category.id}">${category.nameEn}</option>
									</c:if>
								</c:forEach>
							</c:when>
						</c:choose>
					</select>
				</div>


				<div class="column_in_form">
					<label for="bauthor">Количество</label> <input id="bauthor"
						type="text" name="bamount" value="${book_to_edit.amount}" pattern="^([1-9][0-9]?|100)$" title="1-100"/>
				</div>


				<input class="submit" type="submit" value="Доб. / ред. книгу" />


				</form>
				<!------------------------------------------------------------------------------------------------------------------->


				<!-------------------------------------------------ADD AUTHOR FORM-------------------------------------->
				<form method="POST" action="controller?param=add_author">
					<div class="column_in_form">
						<label for="bauthor">Имя автора</label> <input id="bauthor"
							type="text" name="aname" pattern="^((\w|[а-яА-Я ]){1,20})$" title="MAX: 20"/>
					</div>
					<div class="column_in_form">
						<label for="bauthor">Фамилия автора</label> <input id="bauthor"
							type="text" name="asurname" pattern="^((\w|[а-яА-Я ]){1,20})$" title="MAX: 20"/>
					</div>

					<input class="submit" type="submit" value="Добавить автора" />

				</form>
				<!-------------------------------------------------ADD CATEGORY FORM-------------------------------------->
				<form method="POST" action="controller?param=add_category">
					<div class="column_in_form">
						<label for="bauthor">Категория (RU)</label> <input id="bauthor"
							type="text" name="name_ru" pattern="^((\w|[а-яА-Я]|[ .,?:!]){1,40})$" title="MAX: 40"/>
					</div>
					<div class="column_in_form">
						<label for="bauthor">Категория (EN)</label> <input id="bauthor"
							type="text" name="name_en" pattern="^((\w|[а-яА-Я]|[ .,?:!]){1,40})$" title="MAX: 40"/>
					</div>

					<input class="submit" type="submit" value="Добавить категорию" />
				</form>
			</div>
			<!--------------------------------------------------------------------------------------------------------------->