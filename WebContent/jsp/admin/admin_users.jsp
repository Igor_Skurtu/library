<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


		<table class="book_table">
			<caption>Пользователи</caption>
			
			<tr>

				<th class="book_name">Пользователь</th>
				<th class="book_name">E-mail</th>
				<th class="book_del">Бан</th>
				<th class="book_del">Удалить</th>
				
			</tr>

			<c:forEach var="user" items="${sessionScope.all_users}">
				<tr>
					<td>${user.name}&nbsp;${user.surname}</td>
					<td>${user.email}</td>
					<td>
						<a href = "/Library/controller?param=change_ban&id=${user.id}&ban=${user.ban}"> 
							<c:if test="${not user.ban}">
								<div style="background: green">&nbsp;</div>
							</c:if> 
							<c:if test="${user.ban}">
								<div style="background: red">&nbsp;</div>
							</c:if>
						</a>
					</td>
					
					<td><a class="btn_del" href="controller?param=delete_user&userId=${user.id}" /></td>
					
				</tr>
			</c:forEach>

		</table>