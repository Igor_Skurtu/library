<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<!-- CSS -->
<link rel="stylesheet" href="css/style.css">
<title>ВБиблиотеке</title>
</head>
<body>
	<div id="page_layout">
		<header>
			<a href="http://localhost:8080/Library"><img id="logo"
				src="image/logo.png" /></a>
			<div id="flags">
				<a href="controller?param=change_locale&lang=ru"><img
					src="image/flag/ru.png" /></a> <a
					href="controller?param=change_locale&lang=en"><img
					src="image/flag/en.png" /></a>
			</div>
			<a id="logout" href="controller?param=sign_out"> <fmt:message
					key="pages.header.logout" bundle="${rb}" />
			</a>

			<!--navigation variables processing -->
			<c:choose>
				<c:when test="${sessionScope.admin_layout eq 'books'}">
					<c:set var="books_menu_class" value="youarehere" scope="page" />
					<c:remove var="neworders_menu_class" />
					<c:remove var="orders_menu_class" />
					<c:remove var="users_menu_class" />
				</c:when>
				<c:when test="${sessionScope.admin_layout eq 'neworders'}">
					<c:set var="neworders_menu_class" value="youarehere" scope="page" />
					<c:remove var="books_menu_class" />
					<c:remove var="orders_menu_class" />
					<c:remove var="users_menu_class" />
				</c:when>
				<c:when test="${sessionScope.admin_layout eq 'orders'}">
					<c:set var="orders_menu_class" value="youarehere" scope="page" />
					<c:remove var="books_menu_class" />
					<c:remove var="neworders_menu_class" />
					<c:remove var="users_menu_class" />
				</c:when>
				<c:when test="${sessionScope.admin_layout eq 'users'}">
					<c:set var="users_menu_class" value="youarehere" scope="page" />
					<c:remove var="books_menu_class" />
					<c:remove var="neworders_menu_class" />
					<c:remove var="orders_menu_class" />
				</c:when>
			</c:choose>
			<!------------------------------------------------------------------------------------------------->


			<nav id="main_menu">
				<ul>
					<li class="${books_menu_class}"><a
						href="/Library/controller?param=admin_books"><fmt:message
								key="pages.admin.books.menu" bundle="${rb}" /></a></li>
					<li class="${neworders_menu_class}"><a
						href="/Library/controller?param=admin_new_orders"><fmt:message
								key="pages.admin.new_orders.menu" bundle="${rb}" /></a></li>
					<li class="${orders_menu_class}"><a
						href="/Library/controller?param=admin_orders"><fmt:message
								key="pages.admin.orders.menu" bundle="${rb}" /></a></li>
					<li class="${users_menu_class}"><a
						href="/Library/controller?param=admin_users"><fmt:message
								key="pages.admin.users.menu" bundle="${rb}" /></a></li>
				</ul>
			</nav>
		</header>
		<main>
		<p>
			<ctg:userinfo />
		</p>



		<!-----------------------BOOKS LAYOUT----------------------------------------------------------------------->
		<c:if test="${sessionScope.admin_layout eq 'books'}">

			<%@ include file="/jsp/admin/admin_books.jsp"%>
			

		</c:if> <!----------------------------------------------------------------------------------------------------------->


		<!-----------------------NEW ORDERS LAYOUT----------------------------------------------------------------------->
		<c:if test="${sessionScope.admin_layout eq 'neworders'}">


		<%@ include file="/jsp/admin/admin_neworders.jsp"%>	


		</c:if> <!----------------------------------------------------------------------------------------------------------->
		
		

		<!-----------------------ORDERS LAYOUT----------------------------------------------------------------------->
		<c:if test="${sessionScope.admin_layout eq 'orders'}">

			<%@ include file="/jsp/admin/admin_orders.jsp"%>	

		</c:if> <!----------------------------------------------------------------------------------------------------------->



		<!-----------------------USERS LAYOUT----------------------------------------------------------------------->
		<c:if test="${sessionScope.admin_layout eq 'users'}">


			<%@ include file="/jsp/admin/admin_users.jsp"%>	


		</c:if> <!----------------------------------------------------------------------------------------------------------->



		</main>
		<%@ include file="/jsp/common/footer.jsp"%>
	</div>


	<script type="text/javascript">
	//show and hide form
		function show_hide_add_form() {//confirm_register();
			//alert(document.getElementById("add_book_form").style.display);
			document.getElementById("add_book_form").style.display = (document.getElementById("add_book_form").style.display=='block')?'none':'block';
			}
	
	
	
		//confirm deleting
		var del_btns = document.getElementsByClassName("btn_del");
		for (var i = 0; i < del_btns.length; i++) {
			del_btns[i].onclick = function() {
				var conf = confirm("Подтвердите удаление");
				if (!conf) {
					return false;
				}
			};
		}
	</script>
</body>
</html>