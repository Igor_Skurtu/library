<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="/jsp/common/choose_language.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/style.css">
<title><fmt:message key="pages.error.404" bundle="${rb}" /></title>
</head>

<body>

	<div id="page_layout">
		<header>
			<a href="http://localhost:8080/Library"><img id="logo" src="image/logo.png" /></a>
		</header>

	<main>
		<div id="granny_container">
			<img id="error_granny_img" src="image/error.jpg" />
			<p id="granny_says"> <fmt:message key="pages.error.404.message" bundle="${rb}" /> <br> ${error_message}	</p>
			<form id="form_error" action="index.jsp" method="POST">
				<input type="submit" value="&lt;&nbsp;&lt;&nbsp;&lt;&nbsp;&lt;&nbsp;&lt;" />
			</form>
		</div>
		<p class="error"> <fmt:message key="pages.error.404" bundle="${rb}" /> </p>
	</main>
		
	</div>

</body>
</html>