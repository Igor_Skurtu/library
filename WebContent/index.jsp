<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<%@ include file="/jsp/common/choose_language.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=cp1251">
<title><fmt:message key="pages.index.title" bundle="${rb}" /></title>
</head>
<body>

	<c:if test="${not empty sessionScope.user and sessionScope.user.role eq 1}">
		<jsp:forward page="/jsp/user/user.jsp"/>
	</c:if>
	
	<c:if test="${not empty sessionScope.user and sessionScope.user.role eq 0}">
		<jsp:forward page="/jsp/admin/admin.jsp"/>
	</c:if>
	
	<c:if test="${empty sessionScope.user}">
		<jsp:forward page="/jsp/common/login.jsp"/>
	</c:if>
	
	
</body>
</html>