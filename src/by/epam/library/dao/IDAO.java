package by.epam.library.dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface IDAO<T> {

	static final Logger LOGGER = LogManager.getLogger(IDAO.class);
	
	void add(T t) throws DAOException;
	
	ArrayList<T> findAll() throws DAOException;
	
	default void closeStatement(Statement statement) { 
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.error("The object of statement hadn't been closed ", e);
            }
        }
    }
}
