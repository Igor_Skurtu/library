package by.epam.library.dao;

import by.epam.library.entity.Category;

public interface ICategoryDAO extends IDAO<Category> {

}
