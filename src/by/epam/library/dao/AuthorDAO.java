package by.epam.library.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import by.epam.library.entity.Author;
import by.epam.library.pool.ConnectionPool;
import by.epam.library.pool.ProxyConnection;

public class AuthorDAO implements IAuthorDAO {

	private static final String FIND_ALL_AUTHORS = "SELECT `id`, `name`, `surname` FROM `author` ORDER BY `surname`";
	private static final String ADD_AUTHOR = "INSERT INTO `author` (`name`, `surname`) VALUES (?,?)";

	private static AuthorDAO instance = new AuthorDAO();

	private AuthorDAO() {

	}

	public static AuthorDAO getInstance() {
		return instance;
	}

	@Override
	public void add(Author author) throws DAOException {

		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(ADD_AUTHOR);
			preparedStatement.setString(1, author.getName());
			preparedStatement.setString(2, author.getSurname());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQLException in AuthorDAO.add()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}

	}

	@Override
	public ArrayList<Author> findAll() throws DAOException {

		Author author;
		ArrayList<Author> authors = new ArrayList<Author>();
		ProxyConnection connection = null;
		Statement statement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			statement = connection.createStatement();

			ResultSet resultSet = statement.executeQuery(FIND_ALL_AUTHORS);
			while (resultSet.next()) {
				author = new Author();
				author.setId(resultSet.getInt("id"));
				author.setName(resultSet.getString("name"));
				author.setSurname(resultSet.getString("surname"));
				authors.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException("SQLException in AuthorDAO.findAll()", e);
		} finally {
			closeStatement(statement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		return authors;
	}
}
