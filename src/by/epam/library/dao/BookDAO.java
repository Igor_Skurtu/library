package by.epam.library.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import by.epam.library.entity.Book;
import by.epam.library.pool.ConnectionPool;
import by.epam.library.pool.ProxyConnection;

public class BookDAO implements IBookDAO {

	private static final String FIND_ALL_BOOKS = "SELECT `book`.`id` as `bid`, `book`.`title`, `book`.`amount`,"
			+ "`author`.`id` as `aid`, `author`.`name`, `author`.`surname`, `category`.`id` as `cid`, `category`.`name_ru`, `category`.`name_en` "
			+ "FROM `book` "
			+ "JOIN `author` ON `book`.`author` = `author`.`id` "
			+ "JOIN `category` ON `book.`category` = `category`.`id` "
			+ "ORDER BY `surname`";
	
	private static final String SEARCH_AUTHALL_CATALL = "SELECT `book`.`id` as `bid`, `book`.`title`, `book`.`amount`, "
			+ "`author`.`id` as `aid`, `author`.`name`, `author`.`surname`, `category`.`id` as `cid`, `category`.`name_ru`, `category`.`name_en` "
			+ "FROM `book`, `author`, `category` "
			+ "WHERE `book`.`author` = `author`.`id` "
			+ "AND `book`.`category` = `category`.`id` "
			+ "AND `book`.`title` LIKE ? "
			+ "ORDER BY `surname`";

	private static final String SEARCH_AUTHALL_CATONE = "SELECT `book`.`id` as `bid`, `book`.`title`, `book`.`amount`, "
			+ "`author`.`id` as `aid`, `author`.`name`, `author`.`surname`, `category`.`id` as `cid`, `category`.`name_ru`, `category`.`name_en` "
			+ "FROM `book`, `author`, `category` "
			+ "WHERE `book`.`author` = `author`.`id` "
			+ "AND `book`.`category` = `category`.`id` "
			+ "AND `category`.`id` = ? "
			+ "AND `book`.`title` LIKE ? "
			+ "ORDER BY `surname`";

	private static final String SEARCH_AUTHONE_CATALL = "SELECT `book`.`id` as `bid`, `book`.`title`, `book`.`amount`, "
			+ "`author`.`id` as `aid`, `author`.`name`, `author`.`surname`, `category`.`id` as `cid`, `category`.`name_ru`, `category`.`name_en` "
			+ "FROM `book`, `author`, `category` "
			+ "WHERE `book`.`author` = `author`.`id` "
			+ "AND `book`.`category` = `category`.`id` "
			+ "AND `author`.`id` = ? "
			+ "AND `book`.`title` LIKE ? "
			+ "ORDER BY `surname`";

	private static final String	SEARCH_AUTHONE_CATONE = "SELECT `book`.`id` as `bid`, `book`.`title`, `book`.`amount`, "
			+ "`author`.`id` as `aid`, `author`.`name`, `author`.`surname`, `category`.`id` as `cid`, `category`.`name_ru`, `category`.`name_en` "
			+ "FROM `book`, `author`, `category` "
			+ "WHERE `book`.`author` = `author`.`id` "
			+ "AND `book`.`category` = `category`.`id` "
			+ "AND `author`.`id` = ? "
			+ "AND `category`.`id` = ? "
			+ "AND `book`.`title` LIKE ? "
			+ "ORDER BY `surname`";	
	
	
	private static final String ADD_BOOK ="INSERT INTO `book` (`author`, `title`, `category`, `amount`) VALUES (?,?,?,?)";
	private static final String DELETE_BOOK = "DELETE FROM `book` WHERE `id` = ?";
	private static final String BOOK_AI_NULLING = "ALTER TABLE `book` AUTO_INCREMENT = 0";
	private static final String FIND_BOOK_BY_ID = "SELECT * FROM `book` WHERE `id` = ?";
	private static final String EDIT_BOOK = "UPDATE `book` SET `author` = ?, `title` = ?, `category` = ?, `amount` = ? WHERE `id` = ?";
	private static final String DECREASE_AMOUNT = "UPDATE `book` SET `amount` = ? WHERE `id` = ?";
	private static final String INCREASE_AMOUNT = "UPDATE `book` SET `amount` = `amount` + 1 WHERE `id` = ?";

	private static BookDAO instance = new BookDAO();

	private BookDAO() {

	}

	public static BookDAO getInstance() {
		return instance;
	}

	@Override
	public void add(Book book) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(ADD_BOOK);
			
			preparedStatement.setInt(1, book.getAuthor());
			preparedStatement.setString(2, book.getTitle());
			preparedStatement.setInt(3, book.getCategory());
			preparedStatement.setInt(4, book.getAmount());
			
			preparedStatement.executeUpdate();
	
		} catch (SQLException e) {
			throw new DAOException("SQLException in BookDAO.add()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}

	}
	
	
	public void delete(Book book) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(DELETE_BOOK);
			
			preparedStatement.setInt(1, book.getId());
			
			preparedStatement.executeUpdate();
			closeStatement(preparedStatement);
			
			preparedStatement = connection.prepareStatement(BOOK_AI_NULLING); // nulling auto increment of primary key for index resource economy
			preparedStatement.executeUpdate();
	
		} catch (SQLException e) {
			throw new DAOException("SQLException in BookDAO.delete()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}

	}
	
	

	@Override
	public ArrayList<Book> findAll() throws DAOException {

		Book book;
		ArrayList<Book> books = new ArrayList<Book>();
		ProxyConnection connection = null;
		Statement statement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			statement = connection.createStatement();

			ResultSet resultSet = statement.executeQuery(FIND_ALL_BOOKS);
			while (resultSet.next()) {
				book = new Book();
				book.setId(resultSet.getInt("bid"));
				book.setTitle(resultSet.getString("title"));
				book.setAuthor(resultSet.getInt("aid"));
				book.setAuthorString(resultSet.getString("surname") + ", " + resultSet.getString("name"));
				book.setCategory(resultSet.getInt("cid"));
				book.setCategoryStringRu(resultSet.getString("name_ru"));
				book.setCategoryStringEn(resultSet.getString("name_en"));
				book.setAmount(resultSet.getInt("amount"));
				books.add(book);
			}
		} catch (SQLException e) {
			throw new DAOException("SQLException in BookDAO.findAll()", e);
		} finally {
			closeStatement(statement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		return books;
	}
	
	
	public ArrayList<Book> search(String bookTitle, int bookAuthor,
			int bookCategory) throws DAOException {

		Book book;
		ArrayList<Book> books = new ArrayList<Book>();
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		
		String searchString;
		String titleString = "%" + bookTitle + "%";
		

		
		try {
			connection = ConnectionPool.getInstance().getConnection();
			
			
			
			
			if((bookAuthor == -1) && (bookCategory == -1)) {
				
				searchString = SEARCH_AUTHALL_CATALL;
				preparedStatement = connection.prepareStatement(searchString);
				preparedStatement.setString(1, titleString);
				
			} else if ((bookAuthor == -1) && (bookCategory != -1)) {
				
				searchString = SEARCH_AUTHALL_CATONE;
				preparedStatement = connection.prepareStatement(searchString);
				preparedStatement.setInt(1, bookCategory);
				preparedStatement.setString(2, titleString);
				
			} else if ((bookAuthor != -1) && (bookCategory == -1)) {
				
				searchString = SEARCH_AUTHONE_CATALL;
				preparedStatement = connection.prepareStatement(searchString);
				preparedStatement.setInt(1, bookAuthor);
				preparedStatement.setString(2, titleString);
				
			} else if ((bookAuthor != -1) && (bookCategory != -1)) {
				
				searchString = SEARCH_AUTHONE_CATONE;
				preparedStatement = connection.prepareStatement(searchString);
				preparedStatement.setInt(1, bookAuthor);
				preparedStatement.setInt(2, bookCategory);
				preparedStatement.setString(3, titleString);
			}
			
			
			

			ResultSet resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) {
				book = new Book();
				book.setId(resultSet.getInt("bid"));
				book.setTitle(resultSet.getString("title"));
				book.setAuthor(resultSet.getInt("aid"));
				book.setAuthorString(resultSet.getString("surname") + ", " + resultSet.getString("name"));
				book.setCategory(resultSet.getInt("cid"));
				book.setCategoryStringRu(resultSet.getString("name_ru"));
				book.setCategoryStringEn(resultSet.getString("name_en"));
				book.setAmount(resultSet.getInt("amount"));
				books.add(book);
			}
		} catch (SQLException e) {
			throw new DAOException("SQLException in BookDAO.findAll()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		return books;
	}
	
	

	public void edit(Book book) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(EDIT_BOOK);
			
			preparedStatement.setInt(1, book.getAuthor());
			preparedStatement.setString(2, book.getTitle());
			preparedStatement.setInt(3, book.getCategory());
			preparedStatement.setInt(4, book.getAmount());
			preparedStatement.setInt(5, book.getId());
			
			preparedStatement.executeUpdate();
	
		} catch (SQLException e) {
			throw new DAOException("SQLException in BookDAO.edit()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}
	
	public void decreaseAmount(int bookId) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		Book book = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FIND_BOOK_BY_ID);			
			preparedStatement.setInt(1, bookId);			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				book = new Book();
				book.setAmount(resultSet.getInt("amount"));
			}
			
			int bookAmount = book.getAmount();
			
			if(bookAmount > 0) {
				closeStatement(preparedStatement);
				preparedStatement = connection.prepareStatement(DECREASE_AMOUNT);			
				preparedStatement.setInt(1, bookAmount - 1);
				preparedStatement.setInt(2, bookId);			
				preparedStatement.executeUpdate();
			}
	
		} catch (SQLException | NullPointerException e) {
			throw new DAOException("Exception in BookDAO.decreaseAmount()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}
	
public void increaseAmount(int bookId) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(INCREASE_AMOUNT);
			preparedStatement.setInt(1, bookId);
			preparedStatement.executeUpdate();
			
		} catch (SQLException | NullPointerException e) {
			throw new DAOException("Exception in BookDAO.decreaseAmount()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}

public Book findById(int bookId) throws DAOException {
	
	ProxyConnection connection = null;
	PreparedStatement preparedStatement = null;
	Book book = null;
	try {
		connection = ConnectionPool.getInstance().getConnection();
		preparedStatement = connection.prepareStatement(FIND_BOOK_BY_ID);			
		preparedStatement.setInt(1, bookId);			
		ResultSet resultSet = preparedStatement.executeQuery();
		
		if (resultSet.next()) {
			book = new Book();
			book.setAmount(resultSet.getInt("amount"));
		}

	} catch (SQLException | NullPointerException e) {
		throw new DAOException("Exception in BookDAO.findById()", e);
	} finally {
		closeStatement(preparedStatement);
		ConnectionPool.getInstance().returnConnection(connection);
	}
	
	return book;
	
}

}
