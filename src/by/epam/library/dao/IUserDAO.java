package by.epam.library.dao;

import by.epam.library.entity.User;

public interface IUserDAO extends IDAO<User>{
	
	User findUserByEmail(String email) throws DAOException;
	
}
