package by.epam.library.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import by.epam.library.entity.Category;
import by.epam.library.pool.ConnectionPool;
import by.epam.library.pool.ProxyConnection;

public class CategoryDAO implements ICategoryDAO{

	private static final String FIND_ALL_CATEGORIES = "SELECT `id`, `name_ru`, `name_en` FROM `category`";
	private static final String ADD_CATEGORY = "INSERT INTO `category` (`name_ru`, `name_en`) VALUES (?,?)";
	
	private static CategoryDAO instance = new CategoryDAO();

	private CategoryDAO() {

	}

	public static CategoryDAO getInstance() {
		return instance;
	}
	
	@Override
	public void add(Category category) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		
		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(ADD_CATEGORY);
			preparedStatement.setString(1, category.getNameRu());
			preparedStatement.setString(2, category.getNameEn());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQLException in CategoryDAO.add()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}

	@Override
	public ArrayList<Category> findAll() throws DAOException {
		Category category;
		ArrayList<Category> categories = new ArrayList<Category>();
		ProxyConnection connection = null;
		Statement statement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			statement = connection.createStatement();

			ResultSet resultSet = statement.executeQuery(FIND_ALL_CATEGORIES);
			while (resultSet.next()) {
				category = new Category();
				category.setId(resultSet.getInt("id"));
				category.setNameRu(resultSet.getString("name_ru"));
				category.setNameEn(resultSet.getString("name_en"));
				categories.add(category);
			}
		} catch (SQLException e) {
			throw new DAOException("SQLException in CategoryDAO.findAll()", e);
		} finally {
			closeStatement(statement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		return categories;
	}

}
