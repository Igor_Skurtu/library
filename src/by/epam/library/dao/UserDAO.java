package by.epam.library.dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import by.epam.library.entity.User;
import by.epam.library.pool.ConnectionPool;
import by.epam.library.pool.ProxyConnection;
import by.epam.library.service.MD5Encryptor;

public class UserDAO implements IUserDAO {

	private static final String ADD_USER = "INSERT INTO `user` (`name`, `surname`, `email`, `hash`, `role`, `ban`) VALUES (?,?,?,?,?,?)";
	private static final String DELETE_USER = "DELETE FROM `user` WHERE `id` = ?";
	private static final String CHANGE_BAN = "UPDATE `user` SET `ban` = ? WHERE `id` = ?";
	private static final String FIND_USER_BY_EMAIL = "SELECT `id`, `name`, `surname`, `email`, `hash`, `role`, `ban` FROM `user` WHERE `email` = ?";
	private static final String FIND_ALL_USERS = "SELECT `id`, `name`, `surname`, `email`, `hash`, `role`, `ban` FROM `user` WHERE `role` = ?";
	private static final String USER_AI_NULLING = "ALTER TABLE `user` AUTO_INCREMENT = 0";

	private static UserDAO instance = new UserDAO();

	private UserDAO() {

	}

	public static UserDAO getInstance() {
		return instance;
	}
	
	@Override
	public void add(User user) throws DAOException {
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(ADD_USER);
			preparedStatement.setString(1, user.getName());
			preparedStatement.setString(2, user.getSurname());
			preparedStatement.setString(3, user.getEmail());
			String hash = MD5Encryptor.calculateHash(user.getHash());
			preparedStatement.setString(4, hash);
			preparedStatement.setInt(5, 1); // (0 - admin, 1 - user by default)
			preparedStatement.setInt(6, 0); // (0 - normal, 1 - ban)
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.add()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
	}

	@Override
	public ArrayList<User> findAll() throws DAOException {

        User user;
        ArrayList<User> users = new ArrayList<User>();
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(FIND_ALL_USERS);
			preparedStatement.setInt(1, 1); // corresponds to all simple users (non-administrators)
			ResultSet resultSet = preparedStatement.executeQuery();
			
            while (resultSet.next()) {
            	user = new User();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setEmail(resultSet.getString("email"));

				user.setRole(resultSet.getInt("role"));
				user.setBan(resultSet.getInt("ban") == 1);
				users.add(user);
            }
            
            return users;

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.add()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
	}

	public User findUserByEmail(String email) throws DAOException {

		User user = null;
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FIND_USER_BY_EMAIL);
			preparedStatement.setString(1, email);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				user = new User();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setEmail(resultSet.getString("email"));
				user.setHash(resultSet.getString("hash"));
				user.setRole(resultSet.getInt("role"));
				user.setBan(resultSet.getInt("ban") == 1);
			}

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.findUserByEmail()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		return user;
	}

	public void changeBan(int id, boolean ban) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(CHANGE_BAN);
			int inverted_ban = ban ? 0 : 1;
			preparedStatement.setInt(1, inverted_ban);
			preparedStatement.setInt(2, id);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.changeBan()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}

	public void delete(int userId) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(DELETE_USER);
			
			preparedStatement.setInt(1, userId);
			
			preparedStatement.executeUpdate();
			closeStatement(preparedStatement);
			
			preparedStatement = connection.prepareStatement(USER_AI_NULLING); // nulling auto increment of primary key for index resource economy
			preparedStatement.executeUpdate();
	
		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.delete()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}
	
	
}
