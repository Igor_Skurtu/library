package by.epam.library.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import by.epam.library.entity.Order;
import by.epam.library.pool.ConnectionPool;
import by.epam.library.pool.ProxyConnection;

public class OrderDAO implements IOrderDAO {

	private static OrderDAO instance = new OrderDAO();

	private static final String ADD_ORDER = "INSERT INTO `library`.`order` (`user`, `book`, `hall`, `sent`, `proceeded`, `datetime`) VALUES (?,?,?,?,?,?)";
	private static final String MAKE_ORDER = "UPDATE `order` SET `hall` = ?, `sent` = 1 WHERE `id` = ?";
	private static final String PROCESS_ORDER = "UPDATE `order` SET `proceeded` = 1, `datetime` = ? WHERE `id` = ?";
	private static final String DELETE_ORDER = "DELETE FROM `order` WHERE `id` = ?";
	private static final String FIND_ORDER_BY_ID = "SELECT * FROM `order` WHERE `id` = ?";
	private static final String ORDER_AI_NULLING = "ALTER TABLE `order` AUTO_INCREMENT = 0";
	
	private static final String FIND_ORDERS_BY_USER_ID = "SELECT `order`.`id`, `order`.`user`, "
			+ "`order`.`book`, `order`.`hall`, `order`.`sent`, `order`.`proceeded`, `order`.`datetime`, "
			+ "`user`.`name`, `user`.`surname`, `book`.`title`, "
			+ "`author`.`name` as `aname`, `author`.`surname` as `asurname` "
			+ "FROM `order`, `user`, `book`, `author` "
			+ "WHERE `user`= ? "
			+ "AND `order`.`user` = `user`.`id` "
			+ "AND `order`.`book` = `book`.`id` "
			+ "AND `book`.`author` = `author`.`id`";
	
	private static final String FIND_ALL = "SELECT `order`.`id`, `order`.`user`, "
			+ "`order`.`book`, `order`.`hall`, `order`.`sent`, `order`.`proceeded`, `order`.`datetime`, "
			+ "`user`.`name`, `user`.`surname`, `book`.`title`, `book`.`amount`, "
			+ "`author`.`name` as `aname`, `author`.`surname` as `asurname` "
			+ "FROM `order`, `user`, `book`, `author` "
			+ "WHERE `order`.`user` = `user`.`id` "
			+ "AND `order`.`book` = `book`.`id` "
			+ "AND `book`.`author` = `author`.`id`"
			+ "ORDER BY `order`.`datetime` ASC";
	
	private OrderDAO() {

	}

	public static OrderDAO getInstance() {
		return instance;
	}
	
	
	
	@Override
	public void add(Order order) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(ADD_ORDER);
			preparedStatement.setInt(1, order.getUser());
			preparedStatement.setInt(2, order.getBook());
			int hall = order.isHall() ? 1 : 0;
			preparedStatement.setInt(3, hall); 
			int sent = order.isSent() ? 1 : 0;
			preparedStatement.setInt(4, sent);
			int proceeded = order.isProceeded() ? 1 : 0;
			preparedStatement.setInt(5, proceeded);
			preparedStatement.setTimestamp(6, order.getDateTime()); 

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQLException in OrderDAO.add()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}

	@Override
	public ArrayList<Order> findAll() throws DAOException {
		Order order = null;
		ArrayList<Order> orders = new ArrayList<Order>();
		ProxyConnection connection = null;
		Statement statement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(FIND_ALL);
			
			while (resultSet.next()) {
				order = new Order();
				order.setId(resultSet.getInt("id"));
				order.setUser(Integer.parseInt(resultSet.getString("user")));
				order.setUserString(resultSet.getString("surname") + " " + resultSet.getString("name"));
				order.setBook(resultSet.getInt("book"));
				order.setBookAmount(resultSet.getInt("amount"));
				order.setBookString(resultSet.getString("title"));
				order.setAuthorString(resultSet.getString("asurname") + ", " + resultSet.getString("aname"));
				boolean hall = (resultSet.getInt("hall") == 1)? true : false;
				order.setHall(hall);
				boolean sent = (resultSet.getInt("sent") == 1)? true : false;
				order.setSent(sent);
				boolean proceeded = (resultSet.getInt("proceeded") == 1)? true : false;
				order.setProceeded(proceeded);
				order.setDateTime(resultSet.getTimestamp("datetime"));
				
				orders.add(order);
			}

		} catch (SQLException e) {
			throw new DAOException("SQLException in OrderDAO.findAll()", e);
		} finally {
			closeStatement(statement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		return orders;
	}
	
	public Order find(int orderId) throws DAOException {
		Order order = null;
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FIND_ORDER_BY_ID);
			preparedStatement.setInt(1, orderId);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				order = new Order();
				order.setId(resultSet.getInt("id"));
				order.setUser(resultSet.getInt("user"));
				order.setBook(resultSet.getInt("book"));
				boolean hall = (resultSet.getInt("hall") == 1)? true : false;
				order.setHall(hall);
				boolean sent = (resultSet.getInt("sent") == 1)? true : false;
				order.setSent(sent);
				boolean proceeded = (resultSet.getInt("proceeded") == 1)? true : false;
				order.setProceeded(proceeded);
				order.setDateTime(resultSet.getTimestamp("datetime"));
				
			}

		} catch (SQLException e) {
			throw new DAOException("SQLException in OrderDAO.find()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		return order;
	}

	
	
	
	public ArrayList<Order> findByUserId(int id) throws DAOException {
		
		Order order = null;
		ArrayList<Order> orders = new ArrayList<Order>();
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();
			preparedStatement = connection.prepareStatement(FIND_ORDERS_BY_USER_ID);
			preparedStatement.setInt(1, id);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				order = new Order();
				order.setId(resultSet.getInt("id"));
				order.setUser(Integer.parseInt(resultSet.getString("user")));
				order.setUserString(resultSet.getString("surname") + " " + resultSet.getString("name"));
				order.setBook(resultSet.getInt("book"));
				order.setBookString(resultSet.getString("title"));
				order.setAuthorString(resultSet.getString("asurname") + ", " + resultSet.getString("aname"));
				boolean hall = (resultSet.getInt("hall") == 1)? true : false;
				order.setHall(hall);
				boolean sent = (resultSet.getInt("sent") == 1)? true : false;
				order.setSent(sent);
				boolean proceeded = (resultSet.getInt("proceeded") == 1)? true : false;
				order.setProceeded(proceeded);
				order.setDateTime(resultSet.getTimestamp("datetime"));
				
				orders.add(order);
			}

		} catch (SQLException e) {
			throw new DAOException("SQLException in OrderDAO.findByUserId()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		return orders;
	}

	public void make(Order order) throws DAOException {
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(MAKE_ORDER);
			int hall = order.isHall() ? 1 : 0;
			preparedStatement.setInt(1, hall);
			preparedStatement.setInt(2, order.getId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQLException in OrderDAO.make()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}

	public void delete(int orderId) throws DAOException {

		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(DELETE_ORDER);

			preparedStatement.setInt(1, orderId);

			preparedStatement.executeUpdate();
			closeStatement(preparedStatement);
			
			preparedStatement = connection.prepareStatement(ORDER_AI_NULLING); // nulling auto increment of primary key for index resource economy
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQLException in OrderDAO.delete()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
	}

	public void process(int orderId) throws DAOException{
		
		ProxyConnection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionPool.getInstance().getConnection();

			preparedStatement = connection.prepareStatement(PROCESS_ORDER);
			
			preparedStatement.setTimestamp(1, new Timestamp(new GregorianCalendar().getTimeInMillis()));
			preparedStatement.setInt(2, orderId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("SQLException in OrderDAO.process()", e);
		} finally {
			closeStatement(preparedStatement);
			ConnectionPool.getInstance().returnConnection(connection);
		}
		
	}

}
