package by.epam.library.dao;

import by.epam.library.entity.Author;

public interface IAuthorDAO extends IDAO<Author> {

}
