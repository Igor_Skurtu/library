package by.epam.library.dao;

import by.epam.library.entity.Book;

public interface IBookDAO extends IDAO<Book> {

}
