package by.epam.library.constant;

public class PagePathConst {
	
	public static final String USER_PAGE = "/jsp/user/user.jsp";
	public static final String ADMIN_PAGE = "/jsp/admin/admin.jsp";
	public static final String ERROR_PAGE = "/jsp/error/error500.jsp";
	public static final String INDEX_PAGE = "/index.jsp";
	public static final String LOGIN_PAGE = "/jsp/common/login.jsp";
	public static final String REGISTRATION_PAGE = "/jsp/common/registration.jsp";
	public static final String REGISTRATION_SUCCESSFUL_PAGE = "/jsp/common/registration_successful.jsp";
	public static final String SHOW_ALL_BOOKS_PAGE = "/jsp/common/main.jsp";
	public static final String BAN_PAGE = "/jsp/common/ban.jsp";
	
}
