package by.epam.library.pool;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ConnectionPool {
	
	private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
	
	private static ConnectionPool instance = null;
    private ArrayBlockingQueue<ProxyConnection> pool;
    private static Lock lock = new ReentrantLock();                                       
    private static AtomicBoolean connectionPossibleAtomic = new AtomicBoolean(true);                                                       
    private static AtomicBoolean poolIsCreatedAtomic = new AtomicBoolean(false);
    private static final int POOL_SIZE = 3;




    private ConnectionPool() {
        try {
            
        	DriverManager.registerDriver(new com.mysql.jdbc.Driver());

            Properties dbProperties = new Properties();
            InputStream input = new FileInputStream(this.getClass().getClassLoader().getResource("config.properties").getFile());
            dbProperties.load(input);                                                           

            String url = dbProperties.getProperty("url");
            String login = dbProperties.getProperty("login");
            String password = dbProperties.getProperty("password");

            pool = new ArrayBlockingQueue<ProxyConnection>(POOL_SIZE);                                    

            for (int i = 0; i < POOL_SIZE; i++) {
            	ProxyConnection proxyConnection = new ProxyConnection(DriverManager.getConnection(url, login, password));
                pool.offer(proxyConnection);                                               
            }
        } catch (SQLException | IOException e) {
            LOGGER.fatal("Fatal exception in ConnectionPool class ", e);
            throw new RuntimeException("Fatal exception in ConnectionPool class ", e);
        }
    }
    



    public static ConnectionPool getInstance() {

        if (!poolIsCreatedAtomic.get()) {                                                         
            lock.lock();                                                                       
            try{
                if (instance == null) {
                	instance = new ConnectionPool();
                	poolIsCreatedAtomic.set(true);
                }
            } finally {
                lock.unlock();                                                               
            }
        }
        return instance;
    }


    public ProxyConnection getConnection()  {

    	ProxyConnection proxyConnection = null;
        if (connectionPossibleAtomic.get()) {
            try {
            	proxyConnection = pool.take();                                                      
            } catch (InterruptedException e) {
                LOGGER.error("InterruptedException in ConnectionPool.getConnection()", e);
            }
        }
        return proxyConnection;
    }


    /**
     * return a connection to the pool (queue)
     *
     * @param proxyConnection
     */
    public void returnConnection(ProxyConnection proxyConnection) {
        try {
            if (!proxyConnection.isClosed()) {
                pool.add(proxyConnection);
            }
        } catch (SQLException e) {
            LOGGER.error("SQLException in ConnectionPool.returnConnection()", e);
        }
    }



    /**
     * close all connections in pool
     * suitable for using in @see javax.servlet.GenericServlet#destroy()
     */
    public void cleanUp() {

        final int RETURN_WAITING = 200;
        connectionPossibleAtomic.set(false);                                           
        try {
            TimeUnit.MILLISECONDS.sleep(RETURN_WAITING);
            Iterator<ProxyConnection> iterator = pool.iterator();
            while (iterator.hasNext()) {
                ProxyConnection proxyConnection = iterator.next();
                if (proxyConnection != null) {
                	proxyConnection.close();                                                             
                }
                iterator.remove();
            }
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException in ConnectionPool.cleanUp()", e);
        } catch (SQLException e) {
            LOGGER.error("SQLException in ConnectionPool.cleanUp()", e);
        }
    }
}
