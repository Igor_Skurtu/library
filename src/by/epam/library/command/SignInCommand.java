package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Author;
import by.epam.library.entity.Category;
import by.epam.library.entity.Order;
import by.epam.library.entity.User;
import by.epam.library.service.MD5Encryptor;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class SignInCommand implements ActionCommand {
	
	private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = PagePathConst.USER_PAGE;
		Service service = Service.getInstance();
		User user = null;
        try {
        	
        	
        	String email = request.getParameter(EMAIL);
        	String password = request.getParameter(PASSWORD);
        	user = service.findUserByEmail(email);
        	if(user != null) {
        		
        		if(user.getHash().equals(MD5Encryptor.calculateHash(password)) ) {
        			
        			if(user.getBan()) {
        				page = PagePathConst.BAN_PAGE;
        				return page;
        			}
        			
        			
                    HttpSession session = request.getSession();
                    session.setAttribute("user", user);

                    int userId = user.getId();
                    session.setAttribute("userId", userId);
                    
        			if(user.getRole() == 1){ // if user detected
        				request.getSession().setAttribute("user_layout", "books");
        				ArrayList<Order> userOrders  = service.findOrdersByUserId(userId);
        				request.getSession().setAttribute("user_orders", userOrders);
        			}
                    
        			
        			if(user.getRole() == 0){ // if admin detected
        				request.getSession().setAttribute("admin_layout", "neworders"); 
        				page = PagePathConst.ADMIN_PAGE; 
        				ArrayList<Order> allOrders  = service.findAllOrders();
        				request.getSession().setAttribute("all_orders", allOrders);
        				ArrayList<User> users = service.findAllUsers();
        				request.getSession().setAttribute("all_users", users);
        			}
        			
        			//categories authors lists for all user roles
    				ArrayList<Category> categories = service.findAllCategories();
    				request.getSession().setAttribute("categories", categories);
    				ArrayList<Author> authors = service.findAllAuthors();
    				request.getSession().setAttribute("authors", authors);

        			
        		} else {
        			page = PagePathConst.LOGIN_PAGE;
        			request.setAttribute(EMAIL, email);
        			request.setAttribute("incorrect_password", true);
        		}
        		
        	} else {
        		page = PagePathConst.LOGIN_PAGE;
        		request.setAttribute(EMAIL, email);
        		request.setAttribute("user_does_not_exist", true);
        	}
            

        } catch (ServiceException e) {
            page = PagePathConst.ERROR_PAGE;
            request.getSession().setAttribute("error_message", "Error in SignInCommand ");
        }
		
		
		return page;
	}

}
