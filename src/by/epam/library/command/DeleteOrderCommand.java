package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Order;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;


//добавить проверку по ID, чтобы заказы мог удалять только нужный пользователь или админ



public class DeleteOrderCommand implements ActionCommand {

	private static final String USER_ID = "userId";
	private static final String ORDER_ID = "orderId";

	@Override
	public String execute(HttpServletRequest request) {
		
		String page = PagePathConst.INDEX_PAGE;
		
		int userId = (int)request.getSession().getAttribute(USER_ID);
		int orderId = Integer.parseInt(request.getParameter(ORDER_ID));
		
        Service service = Service.getInstance();
        
        try {
             
        	service.deleteOrder(orderId);
			ArrayList<Order> userOrders  = service.findOrdersByUserId(userId);
			request.getSession().setAttribute("user_orders", userOrders);
			
			ArrayList<Order> allOrders  = service.findAllOrders();
			request.getSession().setAttribute("all_orders", allOrders);
        	//request.getSession().removeAttribute("books");
        	//request.getSession().setAttribute("add_to_order_operation_result", true);

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.setAttribute("error_message", "Error in DeleteOrderCommand");
        }
		return page;
	}

}
