package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.User;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;


/**
 * control registration process
 */
public class RegistrationCommand implements ActionCommand {
    
	
	private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
	
    @Override
	public String execute(HttpServletRequest request) {
    	
    	String page;

        page = PagePathConst.REGISTRATION_PAGE;

        User user = new User();
        user.setName(request.getParameter(NAME));                          
        user.setSurname(request.getParameter(SURNAME));
        user.setEmail(request.getParameter(EMAIL));
        user.setHash(request.getParameter(PASSWORD));

        Service service = Service.getInstance();
        try {
                service.addUser(user);
                page = PagePathConst.REGISTRATION_SUCCESSFUL_PAGE;
                return page;

        } catch (ServiceException e) {
            page = PagePathConst.ERROR_PAGE;
            request.setAttribute("error_message", "Error in RegistrationCommand");
        }
        return page;

	    
	}

}
