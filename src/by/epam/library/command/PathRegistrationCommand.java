package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;

public class PathRegistrationCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		
		return PagePathConst.REGISTRATION_PAGE;
	}

}
