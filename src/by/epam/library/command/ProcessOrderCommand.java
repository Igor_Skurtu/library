package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Order;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class ProcessOrderCommand implements ActionCommand {

	private static final String ORDER_ID = "orderId";
    private static final String BOOK_ID = "bookId";
	
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = PagePathConst.INDEX_PAGE;
		
		int orderId = Integer.parseInt(request.getParameter(ORDER_ID));
		int bookId = Integer.parseInt(request.getParameter(BOOK_ID)); 
		
        Service service = Service.getInstance();
        
        try {
             
        	service.processOrder(orderId, bookId);
			
        	ArrayList<Order> allOrders  = service.findAllOrders();
			request.getSession().setAttribute("all_orders", allOrders);
			

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.setAttribute("error_message", "Error in ProcessOrderCommand");
        }
		
		return page;
	}

}
