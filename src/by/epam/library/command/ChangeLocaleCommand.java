package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;

public class ChangeLocaleCommand implements ActionCommand {
	
	private static final String LANGUAGE = "lang";
	
	@Override
    public String execute(HttpServletRequest request) {

        String page = PagePathConst.INDEX_PAGE;
        String language = request.getParameter(LANGUAGE);                           

        switch (language) {
            case "en":
                request.getSession().setAttribute("locale", "en");                  
                break;
            case "ru":
                request.getSession().setAttribute("locale", "ru");
                break;
        }
        return page;
    }

}
