package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Order;
import by.epam.library.entity.User;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class UserOrdersCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		
		//admin don't have access to this page
		if( ((User)request.getSession().getAttribute("user")).getRole() == 0) {
			return PagePathConst.INDEX_PAGE;
		}
		
		String page = PagePathConst.USER_PAGE;
		
		int userId = (int)request.getSession().getAttribute("userId");
		
		request.getSession().setAttribute("user_layout", "orders");
		
		
		Service service = Service.getInstance();
        try {
        	
        	ArrayList<Order> orders = new ArrayList<Order>();
        	orders = service.findOrdersByUserId(userId);
			
        	request.getSession().setAttribute("user_orders", orders);

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.getSession().setAttribute("error_message", "Error in UserOrdersCommand");
        }
		
		return page;
	}

}
