package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.User;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class ChangeBanCommand implements ActionCommand {

	private static final String ID = "id";
	private static final String BAN = "ban";

	@Override
	public String execute(HttpServletRequest request) {

		String page = PagePathConst.INDEX_PAGE;

		Service service = Service.getInstance();

		int id = Integer.parseInt(request.getParameter(ID));
		boolean ban = Boolean.parseBoolean(request.getParameter(BAN));

		try {
			
			service.changeBan(id, ban);
			ArrayList<User> users = service.findAllUsers();
			request.getSession().setAttribute("all_users", users);

		} catch (ServiceException e) {
            page = PagePathConst.ERROR_PAGE;
            request.getSession().setAttribute("error_message", "Error in ChangeBanCommand");
		}
		return page;
	}

}
