package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Order;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class AddToOrderCommand implements ActionCommand {
	
	private static final String USER_ID = "userId";
    private static final String ADD = "add";
	
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = PagePathConst.USER_PAGE;
		
		int userId = (int)request.getSession().getAttribute(USER_ID);
		ArrayList<Integer> booksIds = new ArrayList<Integer>();
		String[] bookIdString = request.getParameterValues(ADD);
		
		for(int i = 0; i < bookIdString.length; i++) {
			booksIds.add(Integer.parseInt(bookIdString[i]));
		}
		
        Service service = Service.getInstance();
        
        try {
             
        	service.addToOrder(userId, booksIds);
			ArrayList<Order> userOrders  = service.findOrdersByUserId(userId);
			request.getSession().setAttribute("user_orders", userOrders);
        	request.getSession().removeAttribute("books");
        	request.getSession().setAttribute("add_to_order_operation_result", true);

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.setAttribute("error_message", "Error in AddToOrderCommand");
        }
		
		return page;
	}

}
