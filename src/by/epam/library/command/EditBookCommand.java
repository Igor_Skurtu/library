package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Book;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class EditBookCommand implements ActionCommand {

	private static final String BOOK_ID = "bookid";
	private static final String BOOK_TITLE = "bname";
    private static final String BOOK_AUTHOR = "bauthor";
    private static final String BOOK_CATEGORY = "bcategory";
    private static final String BOOK_AMOUNT = "bamount";
	
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = PagePathConst.INDEX_PAGE;
		
		int bookId = Integer.parseInt(request.getParameter(BOOK_ID));
		String bookTitle = request.getParameter(BOOK_TITLE);
		int bookAuthor = Integer.parseInt(request.getParameter(BOOK_AUTHOR));
		int bookCategory = Integer.parseInt(request.getParameter(BOOK_CATEGORY));
		int bookAmount = Integer.parseInt(request.getParameter(BOOK_AMOUNT));
		
		Book book = new Book();
		book.setId(bookId);
		book.setTitle(bookTitle);
		book.setAuthor(bookAuthor);
		book.setCategory(bookCategory);
		book.setAmount(bookAmount);
		
		Service service = Service.getInstance();
        try {
        	
        	service.editBook(book);
			
        	request.getSession().setAttribute("add_edit_book_operation_result", true);

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.getSession().setAttribute("error_message", "Error in EditBookCommand");
        }
		
		return page;
	}

}
