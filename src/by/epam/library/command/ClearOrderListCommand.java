package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Order;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class ClearOrderListCommand implements ActionCommand {

	private static final String USER_ID = "userId";
	private static final String ORDER_ID = "orderId";

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = PagePathConst.USER_PAGE;
		
		int userId = (int)request.getSession().getAttribute(USER_ID);
		
        Service service = Service.getInstance();
        
        ArrayList<Order> userOrders = (ArrayList<Order>)request.getSession().getAttribute("user_orders") ;
        
        try {
            
        	for(Order order : userOrders) {
        		if(!order.isSent()) {
        			service.deleteOrder(order.getId());
        		}
        	}
        	
			userOrders  = service.findOrdersByUserId(userId);
			request.getSession().setAttribute("user_orders", userOrders);
        	//request.getSession().removeAttribute("books");
        	//request.getSession().setAttribute("add_to_order_operation_result", true);

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.setAttribute("error_message", "Error in ClearOrderListCommand");
        }
		return page;
	}


}
