package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.User;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class DeleteUserCommand implements ActionCommand {
	private static final String USER_ID = "userId";

	@Override
	public String execute(HttpServletRequest request) {

		String page = PagePathConst.INDEX_PAGE;


		int userId = Integer.parseInt(request.getParameter(USER_ID));

		Service service = Service.getInstance();
		try {

			service.deleteUser(userId);
			ArrayList<User> users = service.findAllUsers();
			request.getSession().setAttribute("all_users", users);

		} catch (ServiceException e) {
			page = PagePathConst.ERROR_PAGE;
			request.getSession().setAttribute("error_message", "Error in DeleteUserCommand");
		}

		return page;
	}
}
