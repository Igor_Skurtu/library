package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Author;
import by.epam.library.entity.Book;
import by.epam.library.entity.Category;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class SearchBooksCommand implements ActionCommand {
	
	private static final String BOOK_TITLE = "bname";
    private static final String BOOK_AUTHOR = "bauthor";
    private static final String BOOK_CATEGORY = "bcategory";
    
	@Override
    public String execute(HttpServletRequest request) {

        String page;
        ArrayList<Book> books;
        ArrayList<Category> categories;
        ArrayList<Author> authors;

        String bookTitle = request.getParameter(BOOK_TITLE);
        int bookAuthor = Integer.parseInt(request.getParameter(BOOK_AUTHOR));
        int bookCategory = Integer.parseInt(request.getParameter(BOOK_CATEGORY));
        
        request.getSession().setAttribute(BOOK_TITLE, bookTitle);
        request.getSession().setAttribute(BOOK_AUTHOR, bookAuthor);
        request.getSession().setAttribute(BOOK_CATEGORY, bookCategory);

        Service service = Service.getInstance();
        try {
            books = service.searchBooks(bookTitle, bookAuthor, bookCategory);
            categories = service.findAllCategories();
            authors = service.findAllAuthors();

            request.getSession().setAttribute("books", books);
            request.getSession().setAttribute("categories", categories);
            request.getSession().setAttribute("authors", authors);
            
            request.getSession().removeAttribute("add_to_order_operation_result");
            request.getSession().removeAttribute("add_edit_book_operation_result");
            request.getSession().removeAttribute("delete_book_operation_result");
            
            page = PagePathConst.INDEX_PAGE;

        } catch (ServiceException e) {
            page = PagePathConst.ERROR_PAGE;
            request.getSession().setAttribute("error_message", "Error in SearchBooksCommand ");
        }
        return page;
    }
}
