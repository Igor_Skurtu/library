package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {

	private static final String PARAM = "param";

	public ActionCommand defineCommand(HttpServletRequest request) {

		String parameter = request.getParameter(PARAM);
		ActionCommand actionCommand;

		if (parameter == null || parameter.isEmpty()) {
			actionCommand = new PathIndexPageCommand();
		} else {
			CommandEnum paramEnum = CommandEnum.valueOf(parameter.toUpperCase());
			actionCommand = paramEnum.getCommand();
		}
		return actionCommand;
	}

}
