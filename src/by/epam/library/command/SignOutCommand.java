package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;

public class SignOutCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		
		request.getSession().invalidate();
		return PagePathConst.INDEX_PAGE;
	}

}
