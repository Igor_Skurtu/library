package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;

public class UserBooksCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {

		String page = PagePathConst.USER_PAGE;
		request.getSession().setAttribute("user_layout", "books");
		return page;
	}

}
