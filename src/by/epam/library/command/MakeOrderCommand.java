package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Order;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class MakeOrderCommand implements ActionCommand {

	private static final String USER_ID = "userId";
	private static final String ORDER_PLACE = "place";
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = PagePathConst.USER_PAGE;
		
		int userId = (int)request.getSession().getAttribute(USER_ID);
		
		
		ArrayList<Order> orders = (ArrayList<Order>)request.getSession().getAttribute("user_orders");
		
		for(Order order: orders) {
			if(!order.isSent()) {
				String id = Integer.toString(order.getId());
				String hall = request.getParameter(ORDER_PLACE + id);
			
				if(hall.equals("0")) {
					order.setHall(false);
				} else {
					order.setHall(true);
				}
			}
		}
		
        Service service = Service.getInstance();
        
        try {
             
        	service.makeOrder(orders);
			ArrayList<Order> userOrders  = service.findOrdersByUserId(userId);
			request.getSession().setAttribute("user_orders", userOrders);

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.setAttribute("error_message", "Error in MakeOrderCommand");
        }
		return page;
	}
}
