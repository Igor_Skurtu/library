package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Category;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class AddCategoryCommand implements ActionCommand {
	
	private static final String NAME_RU = "name_ru";
    private static final String NAME_EN = "name_en";

	@Override
	public String execute(HttpServletRequest request) {
		
		String page = PagePathConst.INDEX_PAGE;
		
		String nameRu = request.getParameter(NAME_RU);
		String nameEn = request.getParameter(NAME_EN);
		
		Service service = Service.getInstance();
		
        try {

        	service.addCategory(nameRu, nameEn);
			ArrayList<Category> categories = service.findAllCategories();
			request.getSession().setAttribute("categories", categories);
			
        	//request.getSession().setAttribute("operation_result", "OK");

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.setAttribute("error_message", "Error in AddCategoryCommand");
        }
		
		return page;
	}

}
