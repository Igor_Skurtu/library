package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Book;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class DeleteBookCommand implements ActionCommand {

	private static final String BOOK_ID = "bookid";

	@Override
	public String execute(HttpServletRequest request) {

		String page = PagePathConst.INDEX_PAGE;


		int bookId = Integer.parseInt(request.getParameter(BOOK_ID));

		Book book = new Book();
		book.setId(bookId);


		Service service = Service.getInstance();
		try {

			service.deleteBook(book);
			request.getSession().setAttribute("delete_book_operation_result", true);
			request.getSession().removeAttribute("books");

		} catch (ServiceException e) {
			request.getSession().setAttribute("delete_book_operation_result", false);
			page = PagePathConst.ERROR_PAGE;
			request.getSession().setAttribute("error_message", "Error in DeleteBookCommand");
		}

		return page;
	}

}
