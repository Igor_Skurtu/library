package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Author;
import by.epam.library.service.Service;
import by.epam.library.service.ServiceException;

public class AddAuthorCommand implements ActionCommand {

	private static final String AUTHOR_NAME = "aname";
    private static final String AUTHOR_SURNAME = "asurname";
	
    @Override
	public String execute(HttpServletRequest request) {
		
    	String page = PagePathConst.INDEX_PAGE;
		
		String authorName = request.getParameter(AUTHOR_NAME);
		String authorSurname = request.getParameter(AUTHOR_SURNAME);
		
		Service service = Service.getInstance();
		
        try {

        	service.addAuthor(authorName, authorSurname);
			ArrayList<Author> authors = service.findAllAuthors();
			request.getSession().setAttribute("authors", authors);
			
        	//request.getSession().setAttribute("operation_result", "OK");

        } catch (ServiceException e) { 
            page = PagePathConst.ERROR_PAGE;
            request.setAttribute("error_message", "Error in AddAuthorCommand");
        }
		
		return page;
	}

}
