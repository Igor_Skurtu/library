package by.epam.library.command;

public enum CommandEnum {
	
    
	ADD_AUTHOR {
        {
            this.command = new AddAuthorCommand();
        }
    },
	ADD_CATEGORY {
        {
            this.command = new AddCategoryCommand();
        }
    },
	ADD_BOOK {
        {
            this.command = new AddBookCommand();
        }
    },
	ADD_TO_ORDER {
        {
            this.command = new AddToOrderCommand();
        }
    },
	ADMIN_BOOKS {
        {
            this.command = new AdminBooksCommand();
        }
    },
	ADMIN_NEW_ORDERS {
        {
            this.command = new AdminNewOrdersCommand();
        }
    },
	ADMIN_ORDERS {
        {
            this.command = new AdminOrdersCommand();
        }
    },
	ADMIN_USERS {
        {
            this.command = new AdminUsersCommand();
        }
    },
	BOOK_TO_EDIT {
        {
            this.command = new BookToEditCommand();
        }
    },
	CHANGE_LOCALE {
        {
            this.command = new ChangeLocaleCommand();
        }
    },
    CHANGE_BAN {
        {
            this.command = new ChangeBanCommand();
        }
    },
    CLEAR_ORDER_LIST {
        {
            this.command = new ClearOrderListCommand();
        }
    },
    DELETE_BOOK {
        {
            this.command = new DeleteBookCommand();
        }
    },
    DELETE_ORDER {
        {
            this.command = new DeleteOrderCommand();
        }
    },
    DELETE_USER {
        {
            this.command = new DeleteUserCommand();
        }
    },
    EDIT_BOOK {
        {
            this.command = new EditBookCommand();
        }
    },
    MAKE_ORDER {
        {
            this.command = new MakeOrderCommand();
        }
    },
    PATH_INDEX_PAGE {
        {
            this.command = new PathIndexPageCommand();
        }
    },
    PATH_REGISTRATION {
        {
            this.command = new PathRegistrationCommand();
        }
    },
    PROC_ORDER {
        {
            this.command = new ProcessOrderCommand();
        }
    },
    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },
    SIGN_IN {
        {
            this.command = new SignInCommand();
        }
    },
    SIGN_OUT {
        {
            this.command = new SignOutCommand();
        }
    },
    SEARCH_BOOKS {
        {
            this.command = new SearchBooksCommand();
        }
    },
    USER_BOOKS {
        {
            this.command = new UserBooksCommand();
        }
    },
    USER_ORDERS {
        {
            this.command = new UserOrdersCommand();
        }
    };
    
    
    ActionCommand command;
    
    public ActionCommand getCommand() {
        return command;
    }
}
