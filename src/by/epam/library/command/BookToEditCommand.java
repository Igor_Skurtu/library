package by.epam.library.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;
import by.epam.library.entity.Book;

public class BookToEditCommand implements ActionCommand {

    private static final String BOOK_ID = "bookid";
    
	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		String page = PagePathConst.INDEX_PAGE;
		

		int bookId = Integer.parseInt(request.getParameter(BOOK_ID));
		
		Book book;
		//search book data just in session books list
		ArrayList<Book> books = (ArrayList<Book>)request.getSession().getAttribute("books");
		for(Book tempBook : books){
			if(tempBook.getId() == bookId) {
				book = tempBook;
				request.getSession().setAttribute("book_to_edit", book);
				return page;
			}
		}
		
		return page;
	}

}
