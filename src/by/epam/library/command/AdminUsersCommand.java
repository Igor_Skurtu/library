package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;

public class AdminUsersCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = PagePathConst.ADMIN_PAGE;
		request.getSession().setAttribute("admin_layout", "users");
		return page;
	}

}
