package by.epam.library.command;

import javax.servlet.http.HttpServletRequest;

import by.epam.library.constant.PagePathConst;

public class PathIndexPageCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		
		return PagePathConst.INDEX_PAGE;
	}

}
