package by.epam.library.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  Includes methods and patterns to validate input data
 */

public class FieldsValidator {
	
	private final static String AMOUNT_PATTERN = "^([1-9][0-9]?|100)$"; //numbers 1-100

    private final static String BOOK_TITLE_PATTERN = "^((\\w|[а-яА-Я]|[ .,?:!]){1,60})$";
    
    private final static String CATEGORY_NAME_PATTERN = "^((\\w|[а-яА-Я]|[ .,?:!]){1,40})$";

    private final static String NAME_SURNAME_PATTERN = "^((\\w|[а-яА-Я ]){1,20})$";

    private final static String EMAIL_PATTERN = "^([-\\w.]{1,20})@([-\\w.]{1,20}\\.)([a-zA-Z]{2,4})$";

    private final static String PASSWORD_PATTERN = "^((\\w){5,20})$";


    /**
     * validates inputed numbers
     *
     * @param number
     * @return boolean
     */
    public static boolean amountMatch(String number) {

        Pattern pattern = Pattern.compile(AMOUNT_PATTERN);
        Matcher matcher = pattern.matcher(number);

        return matcher.matches();
    }


    /**
     * validates inputed book title
     *
     * @param title
     * @return boolean
     */
    public static boolean bookTitleMatch(String title) {

        Pattern pattern = Pattern.compile(BOOK_TITLE_PATTERN);
        Matcher matcher = pattern.matcher(title);

        return matcher.matches();
    }

    
    /**
     * validates inputed category name
     * 
     * @param category
     * @return boolean
     */
    public static boolean categoryNameMatch(String category) {
    	
        Pattern pattern = Pattern.compile(CATEGORY_NAME_PATTERN);
        Matcher matcher = pattern.matcher(category);

        return matcher.matches();
    }

    /**
     * validates inputed user's or author's name or surname
     *
     * @param name
     * @return boolean
     */
    public static boolean nameSurnameMatch(String name) {

        Pattern pattern = Pattern.compile(NAME_SURNAME_PATTERN);
        Matcher matcher = pattern.matcher(name);

        return matcher.matches();
    }


    /**
     * validates inputed mail
     *
     * @param email
     * @return boolean
     */
    public static boolean emailMatch(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }


    /**
     * validates inputed password
     *
     * @param password
     * @return boolean
     */
    public static boolean passwordMatch(String password) {

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }
}
