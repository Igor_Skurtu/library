package by.epam.library.service;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Creates a hash for password
 */

public class MD5Encryptor {

	private static final Logger LOGGER = LogManager.getLogger(MD5Encryptor.class);
	
    /**
     * Calculates hash for given password string in hex-format
     * @param password
     * @return hash
     */
    public static String calculateHash(String password) {

        MessageDigest messageDigest;
        byte[] digest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(password.getBytes("utf-8"));                           
            digest = messageDigest.digest();                                           

        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("NoSuchAlgorithmException in MD5Encryptor class, calculateHash()");

        } catch (UnsupportedEncodingException e) {
            LOGGER.error("UnsupportedEncodingException in MD5Encryptor class, calculateHash()");
        }
        BigInteger bigInteger = new BigInteger(1, digest);
        String hash = bigInteger.toString(16); // using radix 16 (hex)

        return hash;
    }
	
	
}
