package by.epam.library.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import by.epam.library.dao.AuthorDAO;
import by.epam.library.dao.BookDAO;
import by.epam.library.dao.CategoryDAO;
import by.epam.library.dao.DAOException;
import by.epam.library.dao.OrderDAO;
import by.epam.library.dao.UserDAO;
import by.epam.library.entity.Author;
import by.epam.library.entity.Book;
import by.epam.library.entity.Category;
import by.epam.library.entity.Order;
import by.epam.library.entity.User;

public class Service {

	private static Service instance = new Service();

	private Service() {

	}

	public static Service getInstance() {
		return instance;
	}

	public void addUser(User user) throws ServiceException {

		UserDAO userDAO = UserDAO.getInstance();

		try {
			userDAO.add(user);
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.addUser() ", e);
		}
	}

	public User findUserByEmail(String email) throws ServiceException {

		User user = null;
		UserDAO userDAO = UserDAO.getInstance();

		try {
			user = userDAO.findUserByEmail(email);
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.findUserByEmail()", e);
		}
		return user;
	}

	public ArrayList<User> findAllUsers() throws ServiceException {

		UserDAO userDAO = UserDAO.getInstance();
		ArrayList<User> users;
		try {
			users = userDAO.findAll();
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.findAllUsers()", e);
		}
		return users;
	}

	public void changeBan(int id, boolean ban) throws ServiceException {

		UserDAO userDAO = UserDAO.getInstance();

		try {
			userDAO.changeBan(id, ban);
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.findAllUsers()", e);
		}

	}

	public ArrayList<Book> findAllBooks() throws ServiceException {

		BookDAO bookDAO = BookDAO.getInstance();
		ArrayList<Book> books;
		try {
			books = bookDAO.findAll();
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.findAllBooks()", e);
		}
		return books;
	}

	public ArrayList<Category> findAllCategories() throws ServiceException {

		CategoryDAO categoryDAO = CategoryDAO.getInstance();
		ArrayList<Category> categories;
		try {
			categories = categoryDAO.findAll();
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.findAllCategories()", e);
		}
		return categories;
	}


	public void addToOrder(int userId, ArrayList<Integer> booksIds)
			throws ServiceException {

		Order order = null;
		OrderDAO orderDAO = OrderDAO.getInstance();

		try {
			for (Integer bookId : booksIds) {
				order = new Order();
				order.setUser(userId);
				order.setBook(bookId.intValue());
				order.setHall(true); // books delivered in hall - by default
				order.setSent(false); // only add but no sent by default
				order.setProceeded(false); // order is not proceeded yet
				order.setDateTime(new Timestamp(new GregorianCalendar().getTimeInMillis())); // set current datetime

				orderDAO.add(order);
			}

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.addOrder() ", e);
		}

	}

	public void addCategory(String nameRu, String nameEn)
			throws ServiceException {

		Category category = new Category();
		CategoryDAO categoryDAO = CategoryDAO.getInstance();

		try {

			category.setNameRu(nameRu);
			category.setNameEn(nameEn);

			categoryDAO.add(category);

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.addCategory() ", e);
		}
	}

	public void addAuthor(String authorName, String authorSurname)
			throws ServiceException {

		Author author = new Author();
		AuthorDAO authorDAO = AuthorDAO.getInstance();

		try {

			author.setName(authorName);
			author.setSurname(authorSurname);
			authorDAO.add(author);

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.addAuthor() ", e);
		}

	}

	public ArrayList<Author> findAllAuthors() throws ServiceException {

		AuthorDAO authorDAO = AuthorDAO.getInstance();
		ArrayList<Author> authors;
		try {
			authors = authorDAO.findAll();
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.findAllAuthors()", e);
		}
		return authors;
	}
	
	public ArrayList<Order> findOrdersByUserId(int id) throws ServiceException {

		OrderDAO orderDAO = OrderDAO.getInstance();
		ArrayList<Order> orders;
		try {
			orders = orderDAO.findByUserId(id);
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.findOrdersByUserId()", e);
		}
		return orders;
	}
	
	public ArrayList<Order> findAllOrders() throws ServiceException {
		OrderDAO orderDAO = OrderDAO.getInstance();
		ArrayList<Order> orders;
		try {
			orders = orderDAO.findAll();
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.findAllOrders()", e);
		}
		return orders;
	}

	public void addBook(Book book) throws ServiceException {
		
		BookDAO bookDAO = BookDAO.getInstance();

		try {
			
			bookDAO.add(book);

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.addBook() ", e);
		}
		

	}

	public void deleteBook(Book book) throws ServiceException {
			
		BookDAO bookDAO = BookDAO.getInstance();

		try {
			
			bookDAO.delete(book);

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.deleteBook() ", e);
		}
		
	}

	
	public void editBook(Book book) throws ServiceException {

		BookDAO bookDAO = BookDAO.getInstance();

		try {
			
			bookDAO.edit(book);

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.editBook() ", e);
		}
		
	}

	public ArrayList<Book> searchBooks(String bookTitle, int bookAuthor,
			int bookCategory) throws ServiceException {
		
		ArrayList<Book> books = new ArrayList<Book>();
		
		BookDAO bookDAO = BookDAO.getInstance();

		try {
			
			books = bookDAO.search(bookTitle, bookAuthor, bookCategory);

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.searchBooks() ", e);
		}
		return books;
	}

	public void makeOrder(ArrayList<Order> orders) throws ServiceException {

		OrderDAO orderDAO = OrderDAO.getInstance();
		
		try {
			for(Order order: orders) {
				orderDAO.make(order);
			}

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.makeOrder()", e);
		}
		
	}

	public void deleteOrder(int orderId) throws ServiceException {
		
		OrderDAO orderDAO = OrderDAO.getInstance();
		BookDAO bookDAO = BookDAO.getInstance();
		Order order = null;
		
		try {
			order = orderDAO.find(orderId);
			if(order.isProceeded()) {
				bookDAO.increaseAmount(order.getBook());
			}
			orderDAO.delete(orderId);
				

		} catch (DAOException e) {
			throw new ServiceException(	"ServiceException in Service.deleteOrder()", e);
		}
		
	}

	public void processOrder(int orderId, int bookId) throws ServiceException {
		
		OrderDAO orderDAO = OrderDAO.getInstance();
		BookDAO bookDAO = BookDAO.getInstance();
		Book book;
		
		try {
				
			book = bookDAO.findById(bookId);
			if(book.getAmount() > 0) {
				orderDAO.process(orderId);
				bookDAO.decreaseAmount(bookId);
			}
			
			

		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.processOrder()", e);
		}
		
	}

	public void deleteUser(int userId) throws ServiceException {
		

		UserDAO userDAO = UserDAO.getInstance();

		try {
			userDAO.delete(userId);
		} catch (DAOException e) {
			throw new ServiceException(
					"ServiceException in Service.deleteUser() ", e);
		}
	}
	

}
