package by.epam.library.entity;

import java.sql.Timestamp;


public class Order {
	
	private int id;
	private int user;
	private String userString;
	private int book;
	private int bookAmount;
	private String bookString;
	private String authorString;
	private boolean hall;
	private boolean sent;
	private boolean proceeded;
	private Timestamp dateTime;
	
	public Order() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	public String getUserString() {
		return userString;
	}

	public void setUserString(String userString) {
		this.userString = userString;
	}

	public int getBook() {
		return book;
	}

	public void setBook(int book) {
		this.book = book;
	}

	public int getBookAmount() {
		return bookAmount;
	}

	public void setBookAmount(int bookAmount) {
		this.bookAmount = bookAmount;
	}

	public String getBookString() {
		return bookString;
	}

	public void setBookString(String bookString) {
		this.bookString = bookString;
	}

	public String getAuthorString() {
		return authorString;
	}

	public void setAuthorString(String authorString) {
		this.authorString = authorString;
	}

	public boolean isHall() {
		return hall;
	}

	public void setHall(boolean hall) {
		this.hall = hall;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	public boolean isProceeded() {
		return proceeded;
	}

	public void setProceeded(boolean proceeded) {
		this.proceeded = proceeded;
	}

	public Timestamp getDateTime() {
		return dateTime;
	}

	public void setDateTime(Timestamp date) {
		this.dateTime = date;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + book;
		result = prime * result + ((dateTime == null) ? 0 : dateTime.hashCode());
		result = prime * result + (hall ? 1231 : 1237);
		result = prime * result + id;
		result = prime * result + (proceeded ? 1231 : 1237);
		result = prime * result + (sent ? 1231 : 1237);
		result = prime * result + user;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Order))
			return false;
		Order other = (Order) obj;
		if (book != other.book)
			return false;
		if (dateTime == null) {
			if (other.dateTime != null)
				return false;
		} else if (!dateTime.equals(other.dateTime))
			return false;
		if (hall != other.hall)
			return false;
		if (id != other.id)
			return false;
		if (proceeded != other.proceeded)
			return false;
		if (sent != other.sent)
			return false;
		if (user != other.user)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Order [id=");
		builder.append(id);
		builder.append(", user=");
		builder.append(user);
		builder.append(", book=");
		builder.append(book);
		builder.append(", hall=");
		builder.append(hall);
		builder.append(", dateTime=");
		builder.append(dateTime);
		builder.append("]");
		return builder.toString();
	}
	
	
}
