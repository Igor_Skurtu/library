package by.epam.library.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import by.epam.library.entity.User;

@SuppressWarnings("serial")
public class UserInfoTag extends TagSupport {
	
    @Override
    public int doStartTag() throws JspException {

        try {
        	
        	JspWriter out = pageContext.getOut();
            User user = (User)pageContext.getSession().getAttribute("user");
            out.write(user.toString());
                                                  

        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;                                                               
    }
}
