package by.epam.library.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class CopyRightTag extends TagSupport {
	
	private static final String COPYRIGHT_INFO = " © 2016 ";
	
    @Override
    public int doStartTag() throws JspException {

        try {

            JspWriter out = pageContext.getOut();                                       
            out.write(COPYRIGHT_INFO);

        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;                                                               
    }
}
