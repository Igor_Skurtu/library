package by.epam.library.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.library.command.ActionCommand;
import by.epam.library.command.ActionFactory;
import by.epam.library.pool.ConnectionPool;


@WebServlet(name = "Controller", urlPatterns = {"/controller"})
@SuppressWarnings("serial")
public class Controller extends HttpServlet {
       

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}


	/* 
	 * @see javax.servlet.GenericServlet#destroy()
	 */
	public void destroy() {
        super.destroy();
        ConnectionPool.getInstance().cleanUp();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		processRequest(request, response);
        
	}

	/* 
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		processRequest(request, response);

	}
	


	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ActionFactory actionFactory = new ActionFactory();
		ActionCommand command = actionFactory.defineCommand(request); 
		String page = command.execute(request); 
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
		dispatcher.forward(request, response); 
		
	}

}
