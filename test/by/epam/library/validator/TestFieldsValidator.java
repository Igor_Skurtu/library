package by.epam.library.validator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestFieldsValidator {


	@Test
	public void testAmountMatch() {
		
		assertTrue(FieldsValidator.amountMatch("100"));
		assertFalse(FieldsValidator.amountMatch("0"));
	}
	
	@Test
	public void testBookTitleMatch() {
		
		assertTrue(FieldsValidator.bookTitleMatch("ffff:fff? ffffff. 9999, ааа? !!!!! "));
		assertFalse(FieldsValidator.bookTitleMatch("ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt")); //string length = 61
	}

	@Test
	public void testMailMatch() {
		
		assertTrue(FieldsValidator.emailMatch("ivan.iva-nov@email-service.info"));
	}

}
