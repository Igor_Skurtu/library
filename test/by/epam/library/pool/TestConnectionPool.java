package by.epam.library.pool;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * This test check all connections in ConnectionPool 
 * and auxiliary procedures
 *
 */
public class TestConnectionPool {

	private static ConnectionPool connectionPool;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		connectionPool = ConnectionPool.getInstance();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		connectionPool.cleanUp();
	}

	@Test
	public void test() {

		for (int i = 0; i < 3; i++) {
			assertNotNull(connectionPool.getConnection());
		}
	}

}
